<?php get_header(); ?>

	<?php 
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); 

            if( has_post_thumbnail() ) { ?>

                <div class="topimage" style="background-image: url(<?php echo the_post_thumbnail_url('Toppbild'); ?>">
                    <div class="topimage__content">
                        <div class="reference__header">
                            <div class="reference__headline"><?php the_title(); ?></div>

                            <?php
                            $places = get_the_terms(get_the_ID(), 'place');
                            if(!empty($places)) {
                                foreach ($places as $place) {
                                    $places_names[] = $place->name;
                                }
                            ?>
                                <div class="reference__place">ORT: <?php echo implode(', ', $places_names); ?></div>
                            <?php } ?>
                            
                        </div>
                    </div>
                </div>

            <?php } else { ?>

            <?php } ?>
                
                <div class="content reference">
                    <?php nl2br(the_content()); ?>
                </div>

                <?php
                    $images_ids = explode(',', cbird_get_meta(get_the_ID(), 'reference_meta', 'images'));
                    if(!empty($images_ids)) {

                        $left_images = [];
                        $right_images = [];

                        foreach ($images_ids as $image_id) {
                            
                            $image = wp_get_attachment_image_src( $image_id, 'standard' );

                            if($image[1] > $image[2]) {
                                $left_images[] = $image;
                            } else {
                                $right_images[] = $image;
                            }

                        }


                    ?>


                        <div class="reference_gallery">
                            <div class="reference_gallery__left">
                                <?php
                                    if(!empty($left_images)) {
                                        foreach ($left_images as $left_image) {
                                            echo '<img src="' . $left_image[0] . '" />';
                                        }
                                    }
                                ?>
                            </div>

                            <div class="reference_gallery__right">
                                <?php
                                    if(!empty($right_images)) {
                                        foreach ($right_images as $right_image) {
                                            echo '<img src="' . $right_image[0] . '" />';
                                        }
                                    }
                                ?>
                            </div>

                        </div>

                    <?php }


		} // end while
	} // end if
	?>



<?php get_footer('small'); ?>