<?php 
is_wp_loaded();
if( has_post_thumbnail() ) {
    echo '<div class="topimage" style="background-image: url('. get_the_post_thumbnail_url() .'"></div>';
} else {
    echo '<div class="topimage--blank"></div>';
}
