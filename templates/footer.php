<?php is_wp_loaded(); ?>

<footer class="footer">
    <div class="footer__content">
        <div class="footer__links">
            <div class="footer__social">
                <?php if(!empty(THEME_SETTINGS['social_facebook'])) { ?>
                    <a target="_blank" href="<?= THEME_SETTINGS['social_facebook']; ?>">
                        <div class="icon fb"></div>
                    </a>
                <?php } ?>
                <?php if(!empty(THEME_SETTINGS['social_linkedin'])) { ?>
                    <a target="_blank" href="<?= THEME_SETTINGS['social_linkedin']; ?>">
                        <div class="icon ln"></div>
                    </a>
                <?php } ?>                
                <?php if(!empty(THEME_SETTINGS['social_instagram'])) { ?>
                    <a target="_blank" href="<?= THEME_SETTINGS['social_instagram']; ?>">
                        <div class="icon ig"></div>
                    </a>                    
                <?php } ?>
                <?php if(!empty(THEME_SETTINGS['social_vimeo'])) { ?>
                    <a target="_blank" href="<?= THEME_SETTINGS['social_vimeo']; ?>">
                        <div class="icon vm"></div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="footer__content">
        <div class="box">
            <p class="box__text"><?= nl2br(THEME_SETTINGS['footer_box2_text']); ?></p>
            <a href="<?= THEME_SETTINGS['footer_box2_link']; ?>" class="box__link"><?= THEME_SETTINGS['footer_box2_linktext']; ?></a>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>