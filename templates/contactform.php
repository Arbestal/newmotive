<?php is_wp_loaded(); ?>
<div class="contactform__background">

    <form class="contactform" id="contactform">

        <h2 class="contactform__headline">Kontaktformulär</h2>

        <div class="contactform__holder">

            <div class="contactform__inputfields">
                <div class="contactform__row">
                    <input type="text" id="contact_name" required />
                    <label>Namn*</label>
                </div>
                <div class="contactform__row">
                    <input type="email" id="contact_email" required />
                    <label>E-mailadress*</label>
                </div>
                <div class="contactform__row">
                    <input type="text" id="contact_phone" />
                    <label>Telefonnummer</label>
                </div>
            </div>

            <div class="contactform__message">
                <textarea id="contact_message" required></textarea>
                <label>Meddelande*</label>
            </div>

        </div>

    <div class="contactform__footer">
        <input type="checkbox" id="contact_fax" value="0" />
        <div class="contactform__status">
            <div id="contactform__status--success">Tack, ditt meddelande är nu skickat!</div>
        </div>
        <button id="contactform__submit" type="submit">Skicka</button>
    </div>

    </form>

    <div id="contactform__loader">

    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>

    </div>

</div>