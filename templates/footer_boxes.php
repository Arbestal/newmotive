<?php is_wp_loaded(); ?>
<section class="boxes" id="about-us">
    
    <div class="container to-fade-in">
        <div class="box">
        
            <h2 class="box__headline"><?= THEME_SETTINGS['footer_box1_headline']; ?></h2>
            <p class="box__text"><?= nl2br(THEME_SETTINGS['footer_box1_text']); ?></p>
            <a href="<?= THEME_SETTINGS['footer_box1_linktext']; ?>" class="box__link"><?= THEME_SETTINGS['footer_box1_link']; ?></a>
        </div>
    </div>
</section>
<section class="boxes" id="contact-us" >
    <a name="contact-us"></a>
    <div class="container to-fade-in">
        <div class="footer__info">
            <p><?= nl2br(THEME_SETTINGS['contact_info']); ?></p>
            <p>
                <div class="btn footer">
                    <a href="mailto:hej@newmotive.se"><span>hej@newmotive.se</span></a>
               </div>
            </p>
        </div>
        
<?php
$users = get_users();
if(!empty($users)) {
    echo '<div class="contacts">';
    foreach ($users as $key => $user) {
        $user_meta = get_user_meta($user->ID, 'profile_meta', true);
        
        if(isset($user_meta['show']['yes'])) {
            echo '<div class="contact">';
            echo '<h3>' . $user->display_name . '</h3>';
            echo '<p>' . $user_meta['title'] . '</p>';
            echo '<p><a href="mailto:' . $user->user_email . '">'. $user->user_email .'</a></p>';
            echo '<p>' . $user_meta['phone'] . '</p>';
            echo '</div>';
        }
    }
    echo '</div>';
}

?>
    </div>
        
</section>