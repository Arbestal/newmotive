<?php if( !has_post_thumbnail() ) {
	$black_header = true;
}

get_header(); ?>

	<?php 
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); 

			$page_meta = cbird_get_meta(get_the_ID(), 'page_meta' );
			require('templates/slider.php');

			?>

			<div class="container">
				<div class="content content--page">
					<h1><?php the_title(); ?></h1>
					<?php nl2br(the_content()); ?>
				</div>
			</div>

	<?php
		} // end while
	} // end if
	?>


<?php get_footer(); ?>