const mix = require('laravel-mix');

mix.setPublicPath('/')
   .sass('dev/scss/style.scss', 'style.css')
   .js(['dev/js/script.js'], 'js/script.js')
	.options({
       processCssUrls: false,
    });