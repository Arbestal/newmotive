<?php get_header(); ?>
	<div class="services__content">

		<div class="services">

			<?php
				$services = get_posts(array(
					'post_type' => 'service',
					'posts_per_page' => -1,
				));
                $i = 0;
                $number_of_services = count($services);
			?>

			<?php foreach ($services as $key => $service) {
                
                
                $places = get_the_terms($service->ID, 'place');

                ?>
				
				<a href="<?php echo get_the_permalink( $service ); ?>" class="service<?php
                
                if( $i > 5 ) {
                    echo ' service--hidden';
                }
                
                ?>" style="background-image: url(<?php echo get_the_post_thumbnail_url($service); ?>);">
					<div class="service__content">
						<div class="service__filter"></div>
                        <div class="service__text">
                            <h3 class="service__headline service__headline--small"><?php echo $service->post_title; ?></h3>
                            <div class="service__subheadline">ORT: <?php echo $places[0]->name ?? ''; ?></div>
                        </div>
					</div>
				</a>

			<?php $i++; } ?>

		</div>

        <div class="service__loadmoreholder">
            <div id="service__loadmore">Visa fler</div>
        </div>

	</div>


<?php get_footer('small'); ?>