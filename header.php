<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<?php
	if(is_front_page()) { ?>
		<title><?php bloginfo('name'); ?></title>
	<?php } else if(is_404()) { ?>
		<title>Felsida | <?php bloginfo('name'); ?></title>		
	<?php } else if(is_search()) { ?>
		<title>Söksida | <?php bloginfo('name'); ?></title>		
	<?php } else { ?>
		<title><?php echo the_title(); ?> - <?php bloginfo('name'); ?></title>
	<?php } ?>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<?php
	
	/*
	 * Development info
	 */
	global $Cbird;
	
	if(isset($_SERVER['SERVER_NAME'])) {
		if($Cbird->theme_settings['dev_server'] == $_SERVER['SERVER_NAME']) {
			$show_version_number = true;
		}
	}
		
	if(isset($show_version_number)) {
	
		// Just for development
		$theme_info = wp_get_theme();
		$theme_version = $theme_info -> get('Version');
		?>
	
		<div id="version_number">
			<?php echo 'V. ' . $theme_version; ?>
		</div>
	<?php } ?>

	<?php
	global $black_header;
	?>

	<header class="headerholder<?php
	
		if($black_header) {
			echo ' allways-black';
		}

	?>">
		<div class="header">
			<div class="header__menuholder">
				<div class="header__menu"><img class="menu__logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logotyp.svg" />
				<?php
					wp_nav_menu(
						array(
							'theme_location'  => 'mainmenu',
							'menu'            => '',
							'menu_class'      => '',
							'menu_id'         => '',
							'container'       => false,
							'echo'            => true,
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul class="mainmenu">%3$s</ul>',
							'depth'           => 0,
							'walker'          => New Cbird_Walker_Nav_BEM()
						)
					);
				?>
				</div>
				<!-- <div class="header__menutext">Meny</div> -->
				<div class="header__menubutton">
					<div></div>
					<div></div>
					<div></div>
				</div>
				
			</div>
		</div>
		
	</header>
	<div class="container">
		<a href="<?php echo home_url(); ?>" class="header__logo__link">
			<img class="header__logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logotyp.svg" />
		</a>
	</div>