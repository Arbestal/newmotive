# Columbird Starttema V 1.1.1

## Release

#### Nytt i Version 1.1.3 - 21 april 2021
- Lagt till funktion för att ändra gallery metabox

#### Nytt i Version 1.1.3 - 22 mars 2021
- Lagt till metabox för Video 

#### Nytt i Version 1.1.2 - 3 mars 2021
- Fixat fel som uppstår om man bara har checkboxar som metafält och avmarkerar alla och sparar. Om checkboxar var markerad sen tidigare så blev dessa fortfarande markerade.
- Fixat så description visas under titeln i metaboxen
- Fixat så det ser bra ut på mindre skärmar

#### Nytt i Version 1.1.1 - 28 oktober 2020 
- Fixat fel med att information som man skrivit in i metaboxarna försvinner när man skapar en användare och det blir validerings fel och sidan laddas om.

#### Nytt i Version 1.1 - 2 juli 2020 
- Metaboxarna har nu support för dependency
- Metaboxarna kan nu användas på en profilsida
- Nya typer av metafält är tillagda
- Fixat buggen med att metaboxar sparas dubbelt i databasen
- Metafälten sparas nu både som en serialiserad array och som egna rader i databasen
- Ny fil för inställningar, settings.php
- I inställningsfilen kan man nu ange utvecklingsserver (dev_server), där visas versionsnumret
- I inställningsfilen kan man nu ange om css-filen style.css ska cachas eller ej
- I inställningsfilen kan man nu ange om js-filen script.js ska cachas eller ej
- Fixat problem med dependencies i npm
- PHPMailer finns nu med i temat

## Dokumentation

1. Installera beroenden med 'npm install' \
[Kräver Node.js](https://nodejs.org/en/)
(Innan du kör installationen måste du radera mappen node_modules om den redan finns)

2. Kör valfritt scriptet med 'npm run'

### Scripts
- dev
- watch
- hot
- production

Mer info finns här: [https://laravel.com/docs/5.7/mix](https://laravel.com/docs/5.7/mix)

## Om starttemat

- Använder sig utav Laravel-mix 
- Använder sig utav reset.css
- Använder sig utav Psr4 Autoloader
- Har färdig struktur för en enkel sida med header, footer och meny.
- Har egenutvecklat system för att lägga till metaboxar


## Sass 
I dev/scss placeras alla scss-filer.
style.scss används för att inkludera övriga filer.

Via Laravel-mix som är en wrapper till webpack görs filen dev/scss/style.scss om till css och placeras i roten.

I dev/scss/components/ placeras scss-komponenter och tanken är att varje del på hemsidan ska ses som en komponent för att bland annat vara enklare att arbeta med, underhålla och lyftas ut till andra projekt.


## JavaScript
I dev/js placeras alla js-filer
Via Laravel-mix görs dessa om om placeras i roten.


## Webpack / Laravel-mix
I filen webpack.mix.js anger man vilka filer som ska processas och så vidare.

Mer info finns på: [https://laravel.com/docs/5.7/mix](https://laravel.com/docs/5.7/mix)


## Inställningar
Under Inställningar -> Temainställningar i WodPress Admin kan man i dagsläge göra följande:

- Välja bakgrundsfärg och bild på inloggningssidan till WordPress
- Välja vilken sida som ska vara 404-sida
- Välja om "adminbar" ska visas i frontend


## Frontend

Läser in style.css
Läser in js/script.js

Footern placeras alltid längst ner på sidan. Används denna struktur:

```
<header>
</header>

<main>

	Här placeras allt innehåll

</main>

<footer>
</footer>
```

## Meny

Huvudmenyn använder som standard walkern Cbird_Walker_Nav som finns i cbird-includes/classes/Cbird_Walker_Nav. \
Walkern lägger till classer på elementen i menyn som gör det lättare att arbeta med den med css och js. \

Huvudmenyn kan ha upp till 4 nivåer.


## Metaboxar

I dagsläget finns det stöd för följade metaboxar:

- Checkbox
- Colorpicker
- Datepicker
- Timepicker
- File
- Rubrik
- Underrubrik
- Bild
- Galleri
- Radioknapp
- Checkbox
- Text
- Textarea
- Video
- Wysiwyg
- Dropdown
- Grupp

Inställningar för dessa ligger i cbird-includes/metaboxes/fields och det går att skapa egna.

Exempel för hur metaboxarna används finns i filen cbird-includes/cpt/posttype.php

Metaboxarna sparas som en serialiserad array i tabellen options och som vanliga metafält i databasen

## Metaboxar dependency

För metaboxarna kan man även använda sig utav beroenden för vilka som ska visas och inte. Exempel:

```php
array(
	'type' => 'group',
	'dependency' => array('type', '==', 'simple'),
	'fields' => array (

	)
),
```

## Metaboxar för kategorier

Metaboxar går även att använda för kategorier.

Använd klassen CategoryMetaboxes
$category_metabox = New CategoryMetaboxes();
$category_metabox->create()


## Metaboxar för profilsida

Metaboxar går även att använda för profiler

Använd klassen ProfileMetaboxes:
$profile_metabox = New ProfileMetaboxes();
$profile_metabox->create()


## Optionssida

Metaboxar går även att använda för optionssidor

Exempel för hur en optionssida skapas finns i cbird-includes/options/theme.php


## Hjälpfunktioner

cbird_load_cpt('filnamn');
läser in filen: cbird-includes/cpt/filnamn.php där man skapar sin CPT.

cbird_load_option('filnamn');
läser in filen: cbird-includes/options/filnamn.php där man skapar options.

cbird_get_image_src($id, $size);
returnerar src för bild

cbird_get_option($name, $value = false);
returnerar options med $name. 
Är $value infyllt returneras det värdet från options med det värdet

cbird_get_meta($post_id, $key, $value = false);
returnerar metadata för vald post och nyckel.
Är $value infyllt returneras värdet som har det id-värdet

cbird_get_term_meta($term_id);
returnerar metadata för vald kategori


## Todo och ideér

Temainställningar
- Kunna lägga till favicon
Seo inställningar / og
Kunna sortera poster
Kunna styla och skapa "snyggare" mail
Kunna välja vad som ska visas i admin menyn för olika roller
Kunna välja vilka rättigheter roller ska ha
Kunna skapa egna roller