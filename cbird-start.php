<?php
namespace Cbird;

/* 
* CONSTANS
*/

DEFINE('ROOT', dirname(__FILE__));
DEFINE('AJAX_URL', admin_url( 'admin-ajax.php' ));

/* Functions */
require_once(ROOT . '/cbird-includes/functions.php');

/* Psr4 autoloader */
require_once(ROOT . '/cbird-includes/loader/Psr4AutoLoader.php');

$Cbird = New Start();

?>