/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./dev/js/script.js":
/*!**************************!*\
  !*** ./dev/js/script.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  /*
   * Cookie
   */
  var acceptCookies = Cookies.get('acceptCookies');

  if (acceptCookies != "true") {
    $('#cb-cookie-holder').fadeIn();
    $('#cb-cookie-accept').click(function () {
      $('#cb-cookie-holder').fadeOut();
      Cookies.set('acceptCookies', 'true', {
        expires: 365
      });
    });
  }

  $('#contactform').submit(function (e) {
    e.preventDefault();
    $('#contactform__loader').css('display', 'flex').hide().fadeIn(function () {
      $.ajax({
        url: wp_data.ajax_url,
        dataType: 'json',
        type: "POST",
        data: {
          action: 'adamant_contact',
          nonce: wp_data.nonce,
          name: $('#contact_name').val(),
          email: $('#contact_email').val(),
          message: $('#contact_message').val(),
          phone: $('#contact_phone').val(),
          contact: $('#contact_fax').is(':checked')
        },
        success: function success(data) {
          $('#contactform__loader').fadeOut();
          $('#contactform__status--success').fadeIn();
          $('#contact_name').val('');
          $('#contact_phone').val('');
          $('#contact_email').val('');
          $('#contact_message').val('');
          $('#contactform .active').removeClass('active');
          console.log(data);
        },
        error: function error(data) {
          console.log(data);
        }
      });
    });
  });
  $('#service__loadmore').click(function () {
    var toLoad = 6;
    var i = 1;
    $('.service--hidden').each(function () {
      $(this).fadeIn();
      $(this).removeClass('service--hidden');
      i++;

      if (i > toLoad) {
        return false;
      }
    });

    if (!$('.service--hidden').length) {
      $('#service__loadmore').hide();
    }
  });
  $('.header__menuholder').click(function () {
    $(this).toggleClass('open');
  });
  $('.service').on('touchstart click', function () {
    $(this).toggleClass('active');
  });
  $('.slider__content').slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    dots: true
  });

  function checkHeader() {
    if ($(window).scrollTop() > 243) {
      $('.headerholder').addClass('black');
    } else {
      $('.headerholder').removeClass('black');
    }
  }

  $(window).scroll(function () {
    checkHeader();
  });
  checkHeader();
  $('.contactform input, .contactform textarea').keyup(function () {
    if ($(this).val() == '') {
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
    }
  });
  document.querySelectorAll('a[href^="#"]').forEach(function (anchor) {
    anchor.addEventListener('click', function (e) {
      e.preventDefault();
      document.querySelector(this.getAttribute('href')).scrollIntoView({
        behavior: 'smooth'
      });
    });
  });

  function checkElementLocation() {
    var $window = $(window);
    var bottom_of_window = $window.scrollTop() + $window.height();
    $('.to-fade-in').each(function (i) {
      var $that = $(this);
      var bottom_of_object = $that.position().top + $that.outerHeight(); //if element is in viewport, add the animate class

      if (bottom_of_window > bottom_of_object) {
        $(this).addClass('fade-in');
      }
    });
  } // if in viewport, show the animation


  checkElementLocation();
  $(window).on('scroll', function () {
    checkElementLocation();
  });
})(jQuery);

/***/ }),

/***/ "./dev/scss/style.scss":
/*!*****************************!*\
  !*** ./dev/scss/style.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!******************************************************!*\
  !*** multi ./dev/js/script.js ./dev/scss/style.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\xampp\htdocs\newmotive-se\wp-content\themes\newmotive-theme\dev\js\script.js */"./dev/js/script.js");
module.exports = __webpack_require__(/*! C:\xampp\htdocs\newmotive-se\wp-content\themes\newmotive-theme\dev\scss\style.scss */"./dev/scss/style.scss");


/***/ })

/******/ });