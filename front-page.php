<?php get_header(); 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	?>
	<section id="top">
	<div class="container">
<?php
			if( has_post_thumbnail() ) {
    echo '<div class="topimage to-fade-in" style="background-image: url('. get_the_post_thumbnail_url() .'"></div>';
} else {
    echo '<div class="topimage--blank to-fade-in"></div>';
}?>

				<div class="content to-fade-in">
					<h1><?php the_title(); ?></h1>
					<?php nl2br(the_content()); ?>
				</div>
			</div>

		<?php

		//
	} // end while
} // end if
?>
	<div id="projects" class="container">
	<h2 class="services__headline"><a name="projects">Projects</a></h2>

	<div class="services">

	<?php
		$services = get_posts(array(
			'post_type' => 'projects',
			'posts_per_page' => 3,
		));

	?>

	<?php foreach ($services as $key => $service) {
        $servMet = cbird_get_meta($service->ID, 'page_meta', false);
		?>
		
		<div class="service to-fade-in" style="background-image: url(<?php echo get_the_post_thumbnail_url($service); ?>);">
			<?php if ($servMet['client']) {
				echo '<h2';
				if ($servMet['color']=='blackish') {
					echo ' class="blackish"';
				} 
				echo '>'. $servMet['client'] .'</h2>';
			} ?>
			<div class="service__content">
				<div class="service__text">
					<h3 class="service__headline"><?php echo $service->post_title; ?></h3>
					<p class="service__info"><?php echo $service->post_content; ?></p>
				</div>
			</div>
		</div>

	<?php } ?>
	</div>
	</section>
	<section id="clients">
		<div class="container clients">
		<?php 
		$clients = get_posts(array(
			'post_type' => 'clients',
			'posts_per_page' => -1,
		));
		foreach ($clients as $key => $client) { ?>
			<div class="client to-fade-in">
				<img src="<?php echo get_the_post_thumbnail_url($client); ?>);">
			</div>
		<?php } ?>
		</div>
	</section>



<?php get_footer(); ?>