<?php

include('settings.php');
include('cbird-start.php');

/* Featured Image */
// add_theme_support('post-thumbnails', array ('page'));
add_image_size('Toppbild', 1366, 546, true );
add_image_size('Listbild', 400, 400, true );

DEFINE('cbird_text_domain', 'start_theme');

/* Load CPT */
cbird_load_cpt('page');
cbird_load_cpt('projects');
cbird_load_cpt('clients');


/* Load Theme Options */
cbird_load_option('theme-settings');
cbird_load_option('profile');

/* Custom settings for the theme */
//cbird_load_option('option_name');

/* Main menu walker */
include('cbird-includes/classes/Cbird_Walker_Nav_BEM.php');




/* Remove items from the admin */
add_action( 'admin_menu', 'my_remove_menu_pages' );

function my_remove_menu_pages() {
  //remove_menu_page( 'tools.php' );                  //Tools
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'edit.php' );                   //Posts
  // remove_menu_page( 'upload.php' );              //Media
  // remove_menu_page( 'themes.php' );              //Appearance
  // remove_menu_page( 'users.php' );               //Users
  // remove_menu_page( 'options-general.php' );     //Settings


	global $current_user;

  if( !empty($current_user->roles) ){
    foreach ($current_user->roles as $key => $value) {
      if( $value == 'editor' ){
        remove_menu_page( 'options-general.php' );        //Settings
      }
    }
  }

};

$edit_editor_role = get_role( 'editor' );
$edit_editor_role->add_cap( 'manage_options' );

function is_wp_loaded() {
  if( !defined('ABSPATH') ) {
    die();
  }
}

add_action('wp_ajax_adamant_contact', 'adamant_contact');
add_action('wp_ajax_nopriv_adamant_contact', 'adamant_contact');

function adamant_contact() {

  if(!isset($_POST['contact'])) {
    wp_send_json(array(
      'status' => 0,
      'post' => $_POST
    ));
  }

  if($_POST['contact'] != 'false') {
    wp_send_json(array(
      'status' => 0,
      'post' => $_POST
    ));
  }

  $fields = array('name', 'email', 'message');

  foreach ($fields as $field) {

    if(!isset($_POST[$field])) {
      wp_send_json(array(
        'status' => 500,
      ));
    }

    if(empty($_POST[$field])) {
      wp_send_json(array(
        'status' => 500,
      ));
    }
  }

  if ( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
    wp_send_json(array(
      'status' => 500,
    ));
  }


  $name = sanitize_text_field($_POST['name']);
  $email = sanitize_text_field($_POST['email']);
  $phone = sanitize_text_field($_POST['phone'] ?? '');
  $message = sanitize_text_field($_POST['message']);
  
  $html_message = '<h2>Meddelande från kontaktformuläret</h2>
    <p>
    Namn: ' . $name . ' <br />
    Email: ' . $email . ' <br />
    Telefonnummer: ' . $phone . ' <br />
    Meddelande: ' . $message . ' <br />
    </p>
  ';
 
  $headers = array();
  $headers[] = 'Content-Type: text/html; charset=UTF-8';
  $headers[] = 'From: adamantgolvteknik.se <postmaster@mg.columbird.se>';
 
  if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $headers[] = 'Reply-To: ' . $name .' <' . $email . '>';
  }

  $theme_settings = get_option('cbird_theme_settings');
  wp_mail($theme_settings['contactform_reciever'], 'Kontaktformuläret - ' . $subject, $html_message, $headers);

  wp_send_json(array(
    'status' => 1,
  ));

}

function mailtrap($phpmailer) {
  $phpmailer->isSMTP();
     

  $phpmailer->SMTPAuth = true;
  
  $phpmailer->Host = 'smtp.mailgun.org';
  $phpmailer->Port = 587;
  $phpmailer->Username = 'postmaster@mg.columbird.se';
  $phpmailer->Password = 'be0531eefa387083add02dcf64aa06b1-db1f97ba-c3a67d42';
}

add_action('phpmailer_init', 'mailtrap');

DEFINE('THEME_SETTINGS', get_option('cbird_theme_settings'));

function add_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'add_mime_types');