<?php get_header();
// Get theme options

		$options = get_option('cbird_theme_settings');
		if(empty($options['404-page'])) { ?>
			<h1>Kunde inte hitta sidan</h1>
			<p>Sidan du söker finns inte. Är du säker på att du skrivit in rätt adress?<br /><br /><a href="<?php echo home_url(); ?>">Tillbaka till startsidan</a></p>
		<?php 

		} else {
			$error_page = get_post($options['404-page']);
			$page_meta = cbird_get_meta($error_page -> ID, 'page_meta' );
			if(isset($page_meta['images']) && !empty($page_meta['images'])) { ?>

				<div class="slider">
			
					<div class="slider__content">
						
				<?php 
				
				$slides = explode(',', $page_meta['images']);
			
				foreach( $slides as $image_id) { 
					$image_url = wp_get_attachment_url( $image_id );
				?>
					<div style="background-image: url(<?php echo $image_url; ?>);">
			
						<div class="slider__line">
			
							<svg xmlns="http://www.w3.org/2000/svg" width="465.035" height="767.555" viewBox="0 0 465.035 767.555">
							<g id="Group_142" data-name="Group 142" transform="translate(-273.64 -96.943)" style="mix-blend-mode: multiply;isolation: isolate">
								<path id="Path_89" data-name="Path 89" d="M273.64,96.943l187.628.672L738.675,864.5l-175.865-.6L358.6,299.335Z" transform="translate(0 0)" fill="#ff702b"/>
							</g>
							</svg>
			
						</div>
			
					</div>
				<?php } ?>
			
			
				</div>
					<div class="slider__filter"></div>
					<div class="slider__text"><h1><?php echo $page_meta['textarea']; ?></h1></div>
				</div>
			
			<?php }	else { ?>
				
				<?php if( has_post_thumbnail() ) { ?>
			
				<div class="topimage" style="background-image: url(<?php echo the_post_thumbnail_url('Toppbild'); ?>">
				</div>
			
				<?php } else { ?>
				<div class="topimage--blank"></div>
				<?php } ?>
			  
				
			
			<?php } ?>
			?>
			<div class="container">
				<div class="content">
					<h2><?php echo $error_page -> post_title; ?></h2>
					<p><?php echo nl2br($error_page -> post_content); ?></p>
				</div>
			</div>


		<?php } ?>


		<div class="services__background">

<div class="services__content">

<h2 class="services__headline">Våra tjänster</h2>

	<div class="services">

		<?php
			$services = get_posts(array(
				'post_type' => 'service',
				'posts_per_page' => -1,
			));

		?>

		<?php foreach ($services as $key => $service) { ?>
			
			<a href="<?php echo get_the_permalink( $service ); ?>" class="service" style="background-image: url(<?php echo get_the_post_thumbnail_url($service); ?>);">
				<div class="service__content">
					<div class="service__filter"></div>
					<div class="service__text">
						<h3 class="service__headline"><?php echo $service->post_title; ?></h3>
					</div>
				</div>
			</a>

		<?php } ?>

	</div>
</div>

</div>

<?php get_footer(); ?>