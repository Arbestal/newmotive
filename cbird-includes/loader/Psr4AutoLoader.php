<?php
/**
* PSR-4 autoloader
*/

require_once('Psr4AutoloaderClass.php');

// instantiate the loader
$loader = new \Cbird\Loader\Psr4AutoloaderClass();

// register the autoloader
$loader->register();

// register the base directories for the namespace prefix
$loader->addNameSpace('Cbird', ROOT . '/cbird-includes/classes/');

?>