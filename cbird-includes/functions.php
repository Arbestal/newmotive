<?php

/* Get category meta */

function cbird_get_term_meta($term_id) {

	$term_meta = get_term_meta($term_id);
	
		if(isset($term_meta['cbird_meta'][0])) {

			$term_meta = unserialize($term_meta['cbird_meta'][0]) ?? array();
			$term_meta = unserialize($term_meta);
			return $term_meta;
									
		} else {

			return array();

		}

}

/* Get post meta */

function cbird_get_meta($post_id, $key, $value = false) {

	if(!$value) {

		$meta = get_post_meta($post_id, $key, true);
		return $meta;

	} else {

		$meta = get_post_meta($post_id, $key, true);
		return $meta[$value] ?? '';

	}

}

/* Get option */

function cbird_get_option($name, $value = false) {

	if(!$value) {

		$option = get_option($name);
		return $option;

	} else {

		$option = get_option($name);
		return $option[$value] ?? '';

	}

}

/* Create CPT */

function cbird_load_cpt($name) {

require_once(ROOT . '/cbird-includes/cpt/' . $name . '.php');

}

/* Create option */

function cbird_load_option($name) {

require_once(ROOT . '/cbird-includes/options/' . $name . '.php');

}

/* Get image source */

function cbird_get_image_src($id, $size = 'default') {

	$image = wp_get_attachment_image_src($id, $size);
	return $image[0] ?? '';

}

function dump($array) {
	echo'<pre>';
	print_r($array);
	echo'</pre>';
}

function cbird_get_nested_menu($menu_name) {

	$locations = get_nav_menu_locations();
	$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
	$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );

	$menu = [];
	$current_menu = false;
	if(!empty($menuitems)) {
	
		foreach ($menuitems as $item) {
			if($item->url == '#') {
	
				$menu[$item->ID] = array(
					'id' => $item->ID,
					'title' => $item->title,
					'children' => array()
				);
				$current_menu = $item->ID;
				continue;
			}
			if($current_menu) {
				$menu[$current_menu]['children'][] = array(
					'id' => $item->ID,
					'title' => $item->title,
					'url' => $item->url
				);
			}
	
		}
	
	}

	return array_values($menu);

}

function cbird_render_nested_menu($items) {

		if(empty($items)) {
			return false;
		}

		echo '<strong>' . $items['title']  . '</strong>' ?? '';
		
		echo'<ul>';
		if(!empty($items['children'])) {
			foreach ($items['children'] as $item) {
				echo '<li><a href="' . $item['url'] . '">' . $item['title'] . '</a></li>';
			}
		}
        echo'</ul>';
}