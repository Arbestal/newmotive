<?php

class Cbird_Meta_Boxes_Field_Checkbox extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){

 		$this->before();

 		foreach ($this->options() as $name => $title) {
 		
 			echo'<input id="cbird_meta_label_' . $this->id() . '_' . $name . '" type="checkbox" name="' . $this->id() . '[' . $name . ']" value="' . $name . '"';


 			if(in_array($name, $this->value('array'))) {
 				echo " checked";
 			}

 			echo ' />';
 			echo'<label for="cbird_meta_label_' . $this->id() . '_' . $name . '">' . $title . '</label><br>';

 		}
		
		$this->after();

  	}

}
