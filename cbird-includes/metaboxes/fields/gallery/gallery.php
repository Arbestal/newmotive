<?php

class Cbird_Meta_Boxes_Field_gallery extends Cbird\Metaboxes_Field {

  public function __construct($options) {
    parent::__construct($options);
  }

  public function render(){

    $this->before();

      echo '<input type="hidden" 
      id="cbird_meta_gallery_'.$this->id().'"
      name="' . $this->id() . '" 
      value="' . $this->value() . '" 
       />';

       if ($this->value() != '') {
        // Load the images
        // $gallery = wp_get_attachment_gallery_src($this->value(), 'medium');
        // $gallery_src = $gallery[0];
          $gallery = explode(",", $this->value());
          $gallery_src = '';
          if ( isset($gallery) && !empty($gallery) ){
            foreach ( $gallery as $image ):
              $gallery_src .=  '<img style="flex: 0 1; width: 40%;padding: 5px 5px 0;" src="'. cbird_get_image_src($image, 'small') .'">';
            endforeach;
          }
       } else {
        $gallery_src = '';
       }

      echo '<div class="cbird_meta_image_box" style="position: relative;" id="cbird_meta_gallery_box_' . $this->id() . '"><div id="thumb-wrapper" style="display: flex;justify-content: flex-start;align-items: flex-start;padding: 0;flex-wrap: wrap; position: relative;" >'. $gallery_src .'</div></div>';
      echo '<input type="hidden" id="gallery_ids" value="">';
        
      if($this->value() != '') {

        echo'<div class="cbird_meta_button cbird_meta_edit_gallery" data-id="' . $this->id() . '">Ändra bild(er)</div>';
        echo'<div class="cbird_meta_button cbird_meta_remove_gallery" data-id="' . $this->id() . '">Ta bort bild(er)</div>';
      } else {
        echo'<div class="cbird_meta_button cbird_meta_add_gallery" data-id="' . $this->id() . '">Lägg till bild(er)</div>';
        echo'<div class="cbird_meta_button cbird_meta_remove_gallery" style="display: none;" data-id="' . $this->id() . '">Ta bort bild(er)</div>';
      }

      $this->after();

    }

}
  