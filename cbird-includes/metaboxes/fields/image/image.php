<?php

class Cbird_Meta_Boxes_Field_Image extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){

 		$this->before();

  		echo'<input type="hidden" 
  		id="cbird_meta_image_'.$this->id().'"
  		name="' . $this->id() . '" 
  		value="' . $this->value() . '" 
  		 />';

  		 if($this->value() != '') {
  		 	// Load the image
  		 	$image = wp_get_attachment_image_src($this->value(), 'medium');
  		 	$image_src = $image[0];

  		 } else {
  		 	$image_src = '';
  		 }

  		echo'<div class="cbird_meta_image_box" style="background-image: url(' . $image_src . ')" id="cbird_meta_image_box_' . $this->id() . '"></div>';
  		  		
  		if($this->value() != '') {
  			echo'<div class="cbird_meta_button cbird_meta_add_image" data-id="' . $this->id() . '">Ändra bild</div>';
			echo'<div class="cbird_meta_button cbird_meta_remove_image" data-id="' . $this->id() . '">Ta bort bild</div>';
  		} else {
  			echo'<div class="cbird_meta_button cbird_meta_add_image" data-id="' . $this->id() . '">Lägg till bild</div>';
  			echo'<div class="cbird_meta_button cbird_meta_remove_image" style="display: none;" data-id="' . $this->id() . '">Ta bort bild</div>';
  		}

  		$this->after();

  	}

}
