<?php

class Cbird_Meta_Boxes_Field_Radio extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){


		$this->before();
		$output = '';
		$styleoutput = '';

		foreach ($this->attributes() as $att => $atr) {
			switch( $att ) {
				case 'multiple':
				case 'style':
					$styleoutput .= $att .'="'. $atr .'" ';
				case 'placeholder':
					$output .= $att .'="'. $atr .'" ';
					break;
			}
			
		}

 		foreach ($this->options() as $name => $title) {
			echo'<div class="radio"><input data-depend-id="' . $this->dep_id . '" id="cbird_meta_label_' . $this->id() . '_' . $name . '" type="radio" name="' . $this->id() . '" value="' . $name . '"'. $styleoutput.'"';

 			if($this->value() == $name) {
 				echo " checked";
 			}

 			echo ' /><div class="check"><div class="inside"></div></div>';
 			echo'<label for="cbird_meta_label_' . $this->id() . '_' . $name . '" class="radio">' . $title . '</label></div>';

 		}
	
 		$this->after();
  	}

}
