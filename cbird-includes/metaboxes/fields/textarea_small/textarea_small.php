<?php

class Cbird_Meta_Boxes_Field_Textarea_small extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){

 		$this->before();

 		echo '<textarea style="height: 80px;" name="'. $this->id() . '">';
 		echo $this->value();
 		echo '</textarea>';

 		$this->after();

  	}

}
