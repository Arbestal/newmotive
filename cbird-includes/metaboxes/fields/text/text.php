<?php

class Cbird_Meta_Boxes_Field_Text extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){

 		$this->before();

  		echo'<input type="text" 
  		data-depend-id="' . $this->dep_id . '"
  		name="' . $this->id() . '" 
  		value="' . $this->value() . '" 
		id="'. $this->id() .'"';
		if ($this->attributes()) {
			foreach ($this->attributes() as $att => $atr) {
				switch( $att ) {
					case 'style':
					case 'placeholder':
						echo $att .'="'. $atr .'" ';
						break;
				}
				
			}
		}
 		echo '/>';

  		$this->after();

  	}

}
