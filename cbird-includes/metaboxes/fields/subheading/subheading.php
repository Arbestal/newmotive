<?php

class Cbird_Meta_Boxes_Field_Subheading extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){ ?>
 		<div class="cbird_meta_box">
 			<h2 class="cbird-subheadline"><?php echo $this->title(); ?></h2>
 		</div>

  	<?php }

}
