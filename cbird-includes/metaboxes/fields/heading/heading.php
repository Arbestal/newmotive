<?php

class Cbird_Meta_Boxes_Field_Heading extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){ ?>
 		<div class="cbird_meta_box">
 			<h1><?php echo $this->title(); ?></h1>
 		</div>

  	<?php }

}
