<?php

class Cbird_Meta_Boxes_Field_Multiselect extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){

			$uniq_id = rand(0, 10000) . time();

			$this->before();

            $multi = true;
			if ($multi) {
				echo'<div id="bootstrap">';
				echo '<select id="' . $uniq_id . '" name="' . $this->id() . '[]" multiple="multiple">';
			}

			if($this->value() == '') {
				$values = array();
			} else {
				$values = $this->value();
			}

			if(isset($this->query_args()['grouped']) && $this->query_args()['grouped'] == 1) {

				foreach ($this->options() as $name => $item) {
				
					echo '<optgroup label="' . $item['name'] .'">';

					foreach ($item['posts'] as $post) { ?>
						<option value="<?php echo $post->ID; ?>"<?php if(in_array($post->ID, $values)) { echo ' selected'; }; ?>><?php echo $post->post_title; ?></option>
					<?php }

				}

			} else {

				foreach ($this->options() as $post_id => $post_title) { ?>
					<option value="<?php echo $post_id; ?>"<?php if(in_array($post_id, $values)) { echo ' selected'; }; ?>><?php echo $post_title; ?></option>
				<?php }


			}

			echo'</select></div>';
			if ($multi) {
				?>
				<script>
					jQuery(document).ready(function() {
						jQuery('#<?php echo $uniq_id; ?>').multiselect();
					});
				</script>
				<?php
			}
			$this->after();
		}


}
