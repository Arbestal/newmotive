<?php

class Cbird_Meta_Boxes_Field_WYSIWYG extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){

		$this->before();

		/*
		The id of the wp_editor may only contain lowercase letters and underscores. 
		Hyphens will cause the editor to display improperly.
		*/

		$wp_editor_id = str_replace(array('[', ']'), '_', $this->id());

		wp_editor( $this->value(), $wp_editor_id, array(
		'wpautop' => true,
		'drag_drop_upload' => true,
		'tabfocus_elements' => 'content-html,save-post',
		'editor_height' => 300,
		'textarea_name' => $this->id(),
		'tinymce' => array(
			'resize' => true,
			'add_unload_trigger' => false,
		),
	) ); 

  		$this->after();

  	}

}
