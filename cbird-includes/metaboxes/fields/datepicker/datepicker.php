<?php

class Cbird_Meta_Boxes_Field_DatePicker extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){

 		$this->before();

 		wp_register_style('datepicker-css', get_template_directory_uri() . '/cbird-includes/css/datepicker.min.css' );
		wp_register_script('datepicker-js', get_template_directory_uri() . '/cbird-includes/js/datepicker.min.js','jquery',false, false );
		
		wp_enqueue_script('jquery');
		
		wp_enqueue_script('datepicker-js');
		wp_enqueue_style('datepicker-css');

		if ( ! function_exists('wpc_admin_datepicker_js_function') ) {
			function wpc_admin_datepicker_js_function() {
				echo '<script>jQuery(function() { jQuery(".cb-date-picker").datepicker({
					language: "sv-SE"
				}); });</script>';
			}
			add_action('admin_footer', 'wpc_admin_datepicker_js_function');
		};
		
		 
		if ( ! function_exists('wpc_admin_datepicker_css_function') ) {
			function wpc_admin_datepicker_css_function() {
				echo '<style>.cb-date-picker {max-width: 210px;}</style>';
			}
			add_action('admin_footer', 'wpc_admin_datepicker_css_function');
		};

  		echo'<input type="text" 
		class="cb-date-picker"   
		name="' . $this->id() . '" 
  		value="' . $this->value() . '" 
		id="'. $this->id() .'"';
		if ($this->attributes()) {
			foreach ($this->attributes() as $att => $atr) {
				switch( $att ) {
					case 'style':
					case 'placeholder':
						echo $att .'="'. $atr .'" ';
						break;
				}
				
			}
		}
 		echo '/>';

  		$this->after();

  	}

}
