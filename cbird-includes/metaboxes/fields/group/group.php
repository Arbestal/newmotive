<?php

class Cbird_Meta_Boxes_Field_Group extends Cbird\Metaboxes_Field {

	public function __construct($options) {

	        parent::__construct($options);
	}

 	public function render(){

 		// $this->before(false);

   		if( empty($this->dependency) ) { ?>

      <div class="cbird_meta_group" data-depend-id="<?php echo $this->id(); ?>" id="<?php echo $this->id(); ?>">

      <?php } else  { ?>

      <div class="cbird_meta_group" data-depend-id="<?php echo $this->id(); ?>" id="<?php echo $this->id(); ?>" data-controller="<?php echo $this->dependency[0]; ?>" data-condition="<?php echo $this->dependency[1]; ?>" data-value="<?php echo $this->dependency[2]; ?>">

      <?php } 		
		
		// Get saved values
		
		if(isset($GLOBALS['post_id']) && $GLOBALS['post_id'] != "") {
			$this->saved_values = get_post_meta($GLOBALS['post_id'], $this->metakey, true);
			$field['show_default'] = false;
		} else {
			$field['show_default'] = true;
		}

		
		if(!empty($this->fields)) {

			foreach ($this->fields as $field) {

				if(class_exists('Cbird_Meta_Boxes_Field_' . $field['type'])) {


						$className = 'Cbird_Meta_Boxes_Field_' . $field['type'];

						// Check if there is any value saved

						if(isset($field['id']) && isset($this->saved_values[$field['id']])) {
							$field['saved_value'] = $this->saved_values[$field['id']];
						} else {
							$field['saved_value'] = '';
						}

						if(isset($field['id'])){
							$field['dep_id'] = $field['id'];
							$field['id'] = 'cbird_meta[' . $field['id'] . ']';
						}

						// Echo the field
						$theField = New $className($field);

				} else {
					echo "Field dosen't exist";
				}

			}

		}


  		$this->after();

  	}

}
