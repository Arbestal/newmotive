<?php

class Cbird_Meta_Boxes_Field_Colorpicker extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){

 		$this->before();

  		echo'<input type="text" 
  		class="cbird-color-field"
  		name="' . $this->id() . '" 
  		value="' . $this->value() . '" 
  		id="'. $this->id() .'" />';

  		$this->after();

  	}

}
