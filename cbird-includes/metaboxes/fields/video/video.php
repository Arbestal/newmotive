<?php

class Cbird_Meta_Boxes_Field_Video  extends Cbird\Metaboxes_Field {

  public function __construct($options) {
          parent::__construct($options);
  }

  public function render(){

    $this->before();

      echo'<input data-depend-id="' . $this->dep_id . '" type="hidden" 
      id="cbird_meta_video_' .$this->id() .'"
      name="' . $this->id() . '" 
      value="' . $this->value() . '" 
       />';

       if($this->value() != '') {
            $filename = basename( get_attached_file( $this->value() ) );
       } else {
            $filename = '';
       }

        echo '<input type="text" disabled id="cbird_videoname" value="' . $filename . '" />';

      if($this->value() != '') {
        // Load the image
        $image = wp_get_attachment_image_src($this->value(), 'medium');
        $image_src = $image[0];

      } else {
        $image_src = '';
      }
    

      if($this->value() != '') {
        echo'<div class="cbird_meta_button cbird_meta_add_video" data-id="' . $this->id() . '">Change video</div>';
        echo'<div class="cbird_meta_button cbird_meta_remove_video" data-id="' . $this->id() . '">Remove video</div>';
      } else {
        echo'<div class="cbird_meta_button cbird_meta_add_video" data-id="' . $this->id() . '">Add video</div>';
        echo'<div class="cbird_meta_button cbird_meta_remove_video" style="display: none;" data-id="' . $this->id() . '">Remove video</div>';
      }

      if( !empty($this->description()) ){
        echo '<p style="margin-top: 20px; color: #6c7781;">' . $this->description() . '</p>';
      }

      $this->after();

    }

}
