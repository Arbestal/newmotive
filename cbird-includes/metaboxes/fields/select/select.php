<?php

class Cbird_Meta_Boxes_Field_Select extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){


			$this->before();
			
			$multi = null;
			$output = '';
			foreach ($this->attributes() as $att => $atr) {
				switch( $att ) {
					case 'multiple':
						if ($atr == true) {
							$output .= $atr . ' ';
							$multi = true;
						}
						
						break;
					case 'style':
					case 'placeholder':
						$output .= $att .'="'. $atr .'" ';
						break;
				}
				
			}
			if ($multi) {
				echo '<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
				<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>';
				echo '<select class="js-basic-multiple" name="' . $this->id() . '[]" multiple="multiple"';
				
			} else {
				echo '<select data-depend-id="'. $this->dep_id .'" name="' . $this->id() . '"';
			}
			echo $output;
			echo '>';
			// echo 'multi = ' . $multi;

			foreach ($this->options() as $name => $title) {
				
				// echo'<option value="' . $name . '"';

				// if ($this->value() == $name) {
					// echo " selected ";
				// }

				// echo '>' . $title . '</option>';
				echo '<option value="'. $name .'" '. $this->checked( $this->value(), $name, 'selected' ) .'>'. $title . '</option>';
				

			}

			echo'</select>';
			if ($multi) {
				?>
				<script>
					jQuery(document).ready(function() {
						jQuery('.js-basic-multiple').select2();
					});
				</script>
				<?php
			}
			$this->after();
		}


}
