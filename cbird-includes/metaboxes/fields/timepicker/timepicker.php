<?php

class Cbird_Meta_Boxes_Field_TimePicker extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);
	}

 	public function render(){

 		$this->before();

 		wp_register_style('clockpicker-css', get_template_directory_uri() . '/cbird-includes/css/jquery-clockpicker.min.css' );
		wp_register_script('clockpicker-js', get_template_directory_uri() . '/cbird-includes/js/jquery-clockpicker.min.js','jquery',false, false );
		
		wp_enqueue_script('jquery');
		
		wp_enqueue_script('clockpicker-js');
		wp_enqueue_style('clockpicker-css');

		if ( ! function_exists('wpc_admin_clockpicker_js_function') ) {
			function wpc_admin_clockpicker_js_function() {
				echo '<script>jQuery(function() { jQuery(".cb-time-picker").clockpicker({
					align: "left",
    				donetext: "Klar"
				}); });</script>';
			}
			add_action('admin_footer', 'wpc_admin_clockpicker_js_function');
		};
		
		 
		if ( ! function_exists('wpc_admin_clockpicker_css_function') ) {
			function wpc_admin_clockpicker_css_function() {
				echo '<style>.cb-time-picker {max-width: 210px;}</style>';
			}
			add_action('admin_footer', 'wpc_admin_clockpicker_css_function');
		};

  		echo'<input type="text" 
		class="cb-time-picker"   
		name="' . $this->id() . '" 
  		value="' . $this->value() . '" 
		id="'. $this->id() .'"';
		if ($this->attributes()) {
			foreach ($this->attributes() as $att => $atr) {
				switch( $att ) {
					case 'style':
					case 'placeholder':
						echo $att .'="'. $atr .'" ';
						break;
				}
				
			}
		}
 		echo '/>';

  		$this->after();

  	}

}
