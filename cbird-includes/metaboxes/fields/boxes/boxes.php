<?php

class Cbird_Meta_Boxes_Field_Boxes extends Cbird\Metaboxes_Field {

	public function __construct($options) {
	        parent::__construct($options);

	}

 	public function render(){

 		$this->before();
				
 		$this->renderFields();

		$this->after();

  	}



  public function renderFields() {

    
      $i = 0;

      ?>

      <div class="cloner" class="field" style="display: none;">
      <div class="field">
      <div class="field_header">
          <input type="text" name="<?php echo $this->id(); ?>[fields_title][000000]" value="Nytt block" />
          <div class="move_field"><i class="fa fa-arrows"></i></div> 
          <div class="remove_field"><i class="fa fa-trash"></i></div>
          <div class="hide_field"><i class="fa fa-chevron-down"></i></div>
      </div>
      <div class="field_content">
      <?php

	    foreach ($this->fields as $field) {

        // Check if there is any value saved

        $field['saved_value'] = $options[$field['id']] ?? '';
        
        $field['id'] = $this->id() . '[' . $field['id'] . '][000000]';

        if(class_exists('cbird_Meta_Boxes_Field_' . $field['type'])) {

            $className = 'cbird_Meta_Boxes_Field_' . $field['type'];

            // Echo the field
            $theField = New $className($field);

        } else {
          echo "Field dosen't exist";
        }

      }
      
      ?>

      </div>
      </div>
      </div>

      <?php



      echo '<div class="cloner-holder">';



      /*
       * Render existing fields
       */

      $saved_groups = [];
      $numbers_of_fields = count( $this->fields );

      $saved_values = $this->value();

      if(!is_array($saved_values)) {
        $saved_values = array();
      }

      $slask = array_values($saved_values);
      

      	if( !empty($saved_values['fields_title']) ) {

      	foreach ($saved_values['fields_title'] as $i => $value) {
          
          if($i == '000000') {
            continue;
          }
	      			
        ?>
				<div class="field">
          <div class="field_header">
              <input type="text" name="<?php echo $this->id(); ?>[fields_title][<?php echo $i; ?>]" value="<?php if( isset($saved_values['fields_title'][$i]) ) { echo $saved_values['fields_title'][$i]; } else { echo 'Block'; } ?>" />
              <div class="move_field"><i class="fa fa-arrows"></i></div> 
              <div class="remove_field"><i class="fa fa-trash"></i></div>
              <div class="hide_field"><i class="fa fa-chevron-down"></i></div>
          </div>
          <div class="field_content" style="display: none;">
        <?php

				      foreach ($this->fields as $field) {

				        // Check if there is any value saved

				        $field['saved_value'] = $saved_values[$field['id']][$i];
				        
				        $field['id'] = $this->id() . '[' . $field['id'] . '][' . $i . ']';

				        if(class_exists('cbird_Meta_Boxes_Field_' . $field['type'])) {

				            $className = 'cbird_Meta_Boxes_Field_' . $field['type'];

				            // Echo the field
				            $theField = New $className($field);

				        } else {
				          echo "Field dosen't exist";
				        }

				      }
              ?>

				      </div>
              </div>

	      		<?php }

      	} else {
      	
  	}

      echo'</div>';

      echo '<div class="addnew cbird_meta_button">Lägg till</div>';


  	}

}