/* Clear the fields after adding a category */ 

jQuery(document).ajaxSuccess(function(evt, xhr, opts) {

    if (
        xhr.status >= 200
        && xhr.status < 300
        && opts.data
        && /(^|&)action=add-tag($|&)/.test(opts.data)
        && xhr.responseXML
        && $('wp_error', xhr.responseXML).length == 0
    ) {

        // Clear the fields

        jQuery('.cbird_meta_image_box').css('background-image', '');
        jQuery(':input','.cbird_meta_box')
          .not(':button, :submit, :reset')
          .val('')
          .prop('checked', false)
          .prop('selected', false);
        
    }
});
