console.log("admin js");

jQuery(document).ready(function(){

  jQuery('.addnew').click(function(){

    var holder = jQuery(this).closest('.cbird_meta_box_field');

    var clone = holder.find('.cloner').clone();
    clone.attr('id', '');
    clone = clone.html();

    clone = clone.replace(new RegExp('000000', 'g'), Math.floor(Date.now() / 1000));

    console.log(clone);
    holder.find('.cloner-holder').append(clone);
    holder.find('.cloner-holder').find('.field').fadeIn();

  });


  jQuery(document).on('click', '.remove_field', function(){
    jQuery(this).closest('.field').fadeOut(function(){
      jQuery(this).remove();
    });
  });

  jQuery(document).on('click', '.hide_field', function(){
    jQuery(this).parent().next().slideToggle();
  });

  jQuery('.cloner-holder').sortable({
    handle: '.move_field'
  });


  /*
  * jQuery function to add file in metabox
  */
	jQuery('.cbird_meta_add_file').on('click',function(){

		button = jQuery(this);

		if ( typeof wp === 'undefined' || ! wp.media || ! wp.media.gallery ) {
          return;
        }

   		wp_media_frame = wp.media({
          library: {
            type: 'application/pdf'
          }
        });

        // When an image is selected, run a callback.
        wp_media_frame.on( 'select', function() {

          var attachment = wp_media_frame.state().get('selection').first().attributes;
          console.log(attachment);
          button.prev().val(attachment.filename);
          button.prev().prev().val(attachment.id);
          button.text('change file');
          button.next().fadeIn();

        });

        // Open the modal.
        wp_media_frame.open();

  });
 
  /*
   * jQuery function to remove file in metaboxes
   */
	jQuery('.cbird_meta_remove_file').on('click', function(){
		button = jQuery(this);
        button.prev().prev().val('');
        button.prev().prev().prev().val('');
        button.prev().text('Add file');
        button.fadeOut();
	});
  /*
  * jQuery function to add video in metabox
  */
	jQuery('.cbird_meta_add_video').on('click',function(){

		button = jQuery(this);

		if ( typeof wp === 'undefined' || ! wp.media || ! wp.media.gallery ) {
          return;
        }

   		wp_media_frame = wp.media({
          library: {
            type: 'video'
          }
        });

        // When an image is selected, run a callback.
        wp_media_frame.on( 'select', function() {

          var attachment = wp_media_frame.state().get('selection').first().attributes;
          console.log(attachment);
          button.prev().val(attachment.filename);
          button.prev().prev().val(attachment.id);
          button.text('Change video');
          button.next().fadeIn();

        });

        // Open the modal.
        wp_media_frame.open();

  });
 
  /*
   * jQuery function to remove vide0 in metaboxes
   */
	jQuery('.cbird_meta_remove_video').on('click', function(){
		button = jQuery(this);
        button.prev().prev().val('');
        button.prev().prev().prev().val('');
        button.prev().text('Add video');
        button.fadeOut();
	});

/*
 *  jQuery function when you click Add image or Change image
 */
  jQuery(document).on('click', '.cbird_meta_add_image',function(){
		button = jQuery(this);
		if ( typeof wp === 'undefined' || ! wp.media || ! wp.media.gallery ) {
      return;
    }
		wp_media_frame = wp.media({
      library: {
        type: 'image'
      }
    });

    // When image is selected, run a callback.
    wp_media_frame.on( 'select', function() {
      var attachment = wp_media_frame.state().get('selection').first().attributes;
        if ( typeof attachment.sizes.medium !== 'undefined' ) {
          var thumbnail = attachment.sizes.medium.url;  
        } else {
          var thumbnail = attachment.sizes.full.url;
        }

        button.prev().css('background-image', 'url(' + thumbnail + ')')
        button.prev().prev().val(attachment.id);
        button.text('Ändra bild');
        button.next().fadeIn();

    });
    // Open the modal.
    wp_media_frame.open();
	});
/*
*  jQuery function when you click Remove image
*/
	jQuery('.cbird_meta_remove_image').on('click', function(){
	  	  button = jQuery(this);
        button.prev().prev().css('background-image', 'url()')
        button.prev().prev().val('');
        button.prev().text('Lägg till bild');
        button.fadeOut();
	});
/*
*  jQuery function when you click Edit image(s) //Gallery
*/
jQuery('.cbird_meta_edit_gallery').on('click',function () {
  button = jQuery(this);
  if (typeof wp === 'undefined' || !wp.media || !wp.media.gallery) {
      return;
  }

  var images = jQuery('#image_ids').val();
  var gallery_state = images ? 'gallery-edit' : 'gallery-library';
  
  var wp_gal_media_frame = wp.media.frames.wp_media_frame = wp.media({
    title: 'My Gallery', // it has no effect but I really want to change the title
    frame: "post",
    toolbar: 'main-gallery',
    state: gallery_state,
    button: { text: 'Edit' },
    library: {
      type: 'image'
    },
    multiple: true,
    editable: true,
    allowLocalEdits: true,
    displaySettings: true,
    displayUserSettings: true
  });
  wp_gal_media_frame.on('open', function () {
    var images = button.prev().prev().prev().val();
    if (!images)
      return;
      var image_ids = images.split(',');
      var selection = wp_gal_media_frame.state().get('selection');
      image_ids.forEach(function (id) {
        attachment = wp.media.attachment(id);
        attachment.fetch();
        selection.add( attachment ? [ attachment ] : [] );
      });
  });

  wp_gal_media_frame.on('update', function () {
    button2 = jQuery('.cbird_meta_add_gallery');
    var images = jQuery('#image_ids');
    // var thumb_wrapper = jQuery('#thumb-wrapper');
    // var thumb_wrapper = button.prev().find('.thumb-wrapper');
    var thumb_wrapper = button.closest('.cbird_meta_box_field').find('#thumb-wrapper');
    thumb_wrapper.html('');
    var image_urls = [];
    var image_ids = [];
    var library = wp_gal_media_frame.state().get('library');
    library.map(function (image) {
      image = image.toJSON();
      image_urls.push(image.url);
      image_ids.push(image.id);
      thumb_wrapper.append('<img style="flex: 0 1; width: 40%;padding: 5px 5px 0;" src="' + image.url + '" alt="" />');
    });
    images.val(image_ids);
    button.prev().prev().prev().val(image_ids);
    button2.text('Välj bilder');
    button2.next().fadeIn();
  });
  // Open the modal.
  wp_gal_media_frame.open();
});
/*


/*
*  jQuery function when you click Add image(s) //Gallery
*/
  jQuery('.cbird_meta_add_gallery').on('click',function () {
    button = jQuery(this);
    if (typeof wp === 'undefined' || !wp.media || !wp.media.gallery) {
        return;
    }
    var images = jQuery('#image_ids').val();
    var gallery_state = images ? 'gallery-edit' : 'gallery-library';
    var wp_gal_media_frame = wp.media.frames.wp_media_frame = wp.media({
        title: 'My Gallery', // it has no effect but I really want to change the title
        frame: "post",
        toolbar: 'main-gallery',
        state: gallery_state,
        library: {
          type: 'image'
        },
        multiple: true
        
    });
    wp_gal_media_frame.on('open', function () {
      var images = button.prev().prev().prev().val();
      if (!images)
        return;
        var image_ids = images.split(',');
        var library = wp_gal_media_frame.state().get('library');
        image_ids.forEach(function (id) {
          attachment = wp.media.attachment(id);
          attachment.fetch();
          library.add(attachment ? [attachment] : []);
        });
    });

    wp_gal_media_frame.on('update', function () {
      button2 = jQuery('.cbird_meta_add_gallery');
      var images = jQuery('#image_ids');
      // var thumb_wrapper = jQuery('#thumb-wrapper');
      // var thumb_wrapper = button.prev().find('.thumb-wrapper');
      var thumb_wrapper = button.closest('.cbird_meta_box_field').find('#thumb-wrapper');
      thumb_wrapper.html('');
      var image_urls = [];
      var image_ids = [];
      var library = wp_gal_media_frame.state().get('library');
      library.map(function (image) {
        image = image.toJSON();
        image_urls.push(image.url);
        image_ids.push(image.id);
        thumb_wrapper.append('<img style="flex: 0 1; width: 40%;padding: 5px 5px 0;" src="' + image.url + '" alt="" />');
      });
      images.val(image_ids);
      button.prev().prev().prev().val(image_ids);
      button2.text('Ändra bilder');
      button2.next().fadeIn();
    });
    // Open the modal.
    wp_gal_media_frame.open();
  });
/*
*  jQuery function when you click Ta bort bild(er) //Gallery
*/
  jQuery('.cbird_meta_remove_gallery').on('click', function () {
    var txt;
    button = jQuery(this);
    var thumb_wrapper = button.closest('.cbird_meta_box_field').find('#thumb-wrapper');
    var r = confirm("Alla bilder kommer att tas bort från slidern.");
    if (r == true) {
      thumb_wrapper.html('');
      button.prev().prev().prev().prev().val('');
      button.prev().text('Välj bilder');
     } else {
      return;
    }
  });
/*
*  jQuery function for //Colorpicker
*/
  jQuery('.cbird-color-field').wpColorPicker();

/*
 * Dependency
 */


jQuery(document).ready(function() {

  var ruleset = jQuery.deps.createRuleset();

  jQuery('[data-controller]').each(function(){

       element = jQuery(this);
 
       controllers = jQuery(this).attr('data-controller').split('|');
       conditions = jQuery(this).attr('data-condition').split('|');
       values = jQuery(this).attr('data-value').split('|');

       jQuery(controllers).each(function(index, value){

           var newRule = ruleset.createRule('[data-depend-id="' + controllers[index] + '"]', conditions[index], values[index]);
           newRule.include(element);

       });

  }); // End of each                

  ruleset.install({ 
       log: true,
       show: function( el ) {
           el.slideDown();
       },
       hide: function( el ) {
           el.slideUp();
       },
   });
   
});
});