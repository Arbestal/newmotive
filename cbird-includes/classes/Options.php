<?php

namespace Cbird;

Class Options {

	public $title;
	public $capability;
	public $slug;
	public $icon;
	public $fields;
	public $id;

	public function __construct($options) {
		$this -> fields = $options['fields'] ?? array();
		$this -> title = $options['title'] ?? '';
		$this -> capability = $options['capability'] ?? 'edit_pages';
		$this -> slug = $options['slug'] ?? '';
		$this -> icon = $options['icon'] ?? '';
		$this -> id = $options['id'] ?? '';
		$this -> type = $options['type'] ?? '';
		$this -> position = $options['position'] ?? 20;
		$this -> parentName = $options['parent'] ?? '';
		
        add_action('admin_init',function() {
            register_setting($this -> id, $this -> id);
        });

        add_action( 'admin_enqueue_scripts', 'wp_enqueue_media' );
        add_action('admin_menu', array(&$this, 'create_menu_page'));

	}

	public function create_menu_page(){

	    if( $this->type == 'submenu' ) {
	      add_submenu_page( $this->parentName, $this->title, $this->title, $this->capability, $this->slug, array( &$this, 'render' ) );

	    } else if( $this->type == 'management' ) {
	      add_management_page( $this->title, $this->title, $this->capability, $this->slug, array( &$this, 'render' ), $this->icon, $this->position );

	    } else if( $this->type == 'dashboard' ) {
	      add_dashboard_page( $this->title, $this->title, $this->capability, $this->slug, array( &$this, 'render' ), $this->icon, $this->position );

	    } else if( $this->type == 'options' ) {
	      add_options_page( $this->title, $this->title, $this->capability, $this->slug, array( &$this, 'render' ), $this->icon, $this->position );

	    } else if( $this->type == 'plugins' ) {
	      add_plugins_page( $this->title, $this->title, $this->capability, $this->slug, array( &$this, 'render' ), $this->icon, $this->position );

	    } else if( $this->type == 'theme' ) {
	      add_theme_page( $this->title, $this->title, $this->capability, $this->slug, array( &$this, 'render' ), $this->icon, $this->position );

	    } else {
	      add_menu_page( $this->title, $this->title, $this->capability, $this->slug, array( &$this, 'render' ), $this->icon, $this->position );

	    }

	}

	public function render() {

		// Get saved values
		
		$screen = get_current_screen();

        if( $screen->parent_base != 'options-general' && isset($_GET['settings-updated']) ) { ?>
            <div id='message' class='updated'>
                <p><strong><?php _e('Settings saved.') ?></strong></p>
            </div>
        <?php } ?>

        <form method="post" action="options.php">

        <?php 
        settings_fields( $this->id );
        do_settings_sections( $this->id );
        $options=get_option( $this->id );

		if(!empty($this->fields)) {

			foreach ($this->fields as $field) {

				// Check if there is any value saved
				if( isset($field['id']) ){
					$field['saved_value'] = $options[$field['id']] ?? '';
					$field['id'] = $this->id . '[' . $field['id'] . ']';
				}

				if(class_exists('Cbird_Meta_Boxes_Field_' . $field['type'])) {

						$className = 'Cbird_Meta_Boxes_Field_' . $field['type'];

						// Echo the field
						$theField = New $className($field);

				} elseif( $field['type'] == 'section-start'){
					echo '<section class="cbird_meta_box_section">';
					echo '<h2>' . $field['title'] . '</h2>';

				} elseif( $field['type'] = 'section-end'){
					echo '</section>';

				} else {
					echo "Field dosen't exist";
				}

			}

		}

		submit_button();

	}

}