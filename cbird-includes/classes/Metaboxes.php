<?php

namespace Cbird;

Class Metaboxes {
	
	private $options;
	private $metakey;
	private $saved_values = array();

	public function __construct() {

	}

	public function create($options) {

		$this->options = $options;
		$this->metakey = $options['id'];


		add_action('add_meta_boxes', array(&$this,'init_metaboxes')); 
	    add_action( 'save_post', array(&$this,'save'), 10, 2 );

	}

	public function init_metaboxes(){

		add_meta_box( $this->options['id'], $this->options['title'], array(&$this,'render'), $this->options['post-type'], $this->options['type']);

	}

	public function render() {

		// Get saved values
		
		if(isset($GLOBALS['post_id']) && $GLOBALS['post_id'] != "") {
			$this->saved_values = get_post_meta($GLOBALS['post_id'], $this->metakey, true);
			$field['show_default'] = false;
		} else {
			$field['show_default'] = true;
		}

		
		if(!empty($this->options['fields'])) {


			foreach ($this->options['fields'] as $field) {

				if(class_exists('Cbird_Meta_Boxes_Field_' . $field['type'])) {


						$className = 'Cbird_Meta_Boxes_Field_' . $field['type'];

						// Check if there is any value saved

						$field['metakey'] = $this->metakey;

						if(isset($field['id']) && isset($this->saved_values[$field['id']])) {
							$field['saved_value'] = $this->saved_values[$field['id']];
						} else {
							$field['saved_value'] = '';
						}

						if(isset($field['id'])){
							$field['dep_id'] = $field['id'];
							$field['id'] = 'cbird_meta[' . $field['id'] . ']';
						}

						// Echo the field
						$theField = New $className($field);

				} else {
					echo "Field dosen't exist";
				}

			}

		}

	}

	public function save($post_id, $post) {

		global $wpdb;

		/*
		 * Check posttype
		 */
		 
		if($post->post_type != $this->options['post-type']){
            return $post_id;
        }

		if($post->post_status == 'trash' or $post->post_status == 'auto-draft'){
            return $post_id;
        }
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }

		$to_keep = array();

        if(isset($_POST['cbird_meta'])) {
      		update_post_meta( $post_id, $this->metakey, $_POST['cbird_meta']);

      		foreach ($_POST['cbird_meta'] as $key => $value) {
      			
      			if(is_array($value)) {

					foreach ($value as $current_key => $current_value) {

						update_post_meta( $post_id, $this->metakey . '-'  . $key . '[' . $current_value . ']', $current_value);
						$to_keep[] = $this->metakey . '-'  . $key . '[' . $current_value . ']';
					    
					}      				


      			} else {

					update_post_meta( $post_id, $this->metakey . '-'  . $key, $value);
					$to_keep[] = $this->metakey . '-'  . $key;

      			}

      		}

        }

        if(empty($to_keep)) {

			$sql = "DELETE FROM wp_postmeta WHERE post_id = " . $post_id . " AND meta_key = '" . $this->metakey . "'";
			$results = $wpdb->get_results($sql);

        	$sql = "DELETE FROM wp_postmeta WHERE post_id = " . $post_id . " AND meta_key LIKE '" . $this->metakey ."-%'";
			$results = $wpdb->get_results($sql);

        } else {

        	foreach ($to_keep as $key => $current) {
        		$to_keep[$key] = "'" . $current . "'";
        	}

        	$to_keep_string = implode(',', $to_keep);

        	$sql = "DELETE FROM wp_postmeta WHERE post_id = " . $post_id . " AND meta_key LIKE '" . $this->metakey ."-%' WHERE meta_key NOT IN(" . $to_keep_string . ")";
			$results = $wpdb->get_results($sql);

        }
       
        return true;


        if( isset($_POST['cbird_save_meta']) ) {

        	foreach ($_POST['cbird_save_meta'] as $array_key => $meta_key) {

        		if(is_array($meta_key)) {

        			foreach ($meta_key as $current_key => $value) {
        				
        				if(isset($_POST['cbird_meta'][$array_key][$current_key])) {
        					update_post_meta( $post_id, $this->metakey . '-'  . $array_key . '[' . $current_key . ']', $value);		
        				} else {
        					update_post_meta( $post_id, $this->metakey . '-'  . $array_key . '[' . $current_key . ']', 'false');	
        				}

        			}

        		} else {

					update_post_meta( $post_id, $this->metakey . '-'  . $meta_key, $_POST['cbird_meta'][$meta_key]);

        		}
        		
        	}

        }


	}

  public function get_options_by_query($query_type, $query_args = array()) {

       // Check for query_args 


      if(empty($query_args)) {

        return array();  

      } else {


      $options = [];


      switch( $query_type ) {

      case 'pages':
      case 'page':

        $pages = get_pages( $query_args );

        if ( ! is_wp_error( $pages ) && ! empty( $pages ) ) {
          foreach ( $pages as $page ) {
            $options[$page->ID] = $page->post_title;
          }
        }

      break;

      case 'posts':
      case 'post':

        $posts = get_posts( $query_args );

        if ( ! is_wp_error( $posts ) && ! empty( $posts ) ) {
          foreach ( $posts as $post ) {
            $options[$post->ID] = $post->post_title;
          }
        }

      break;

      case 'categories':
      case 'category':

        $categories = get_categories( $query_args );

        if ( ! is_wp_error( $categories ) && ! empty( $categories ) && ! isset( $categories['errors'] ) ) {
          foreach ( $categories as $category ) {
            $options[$category->term_id] = $category->name;
          }
        }

      break;

      case 'tags':
      case 'tag':

        $taxonomies = ( isset( $query_args['taxonomies'] ) ) ? $query_args['taxonomies'] : 'post_tag';
        $tags = get_terms( $taxonomies, $query_args );

        if ( ! is_wp_error( $tags ) && ! empty( $tags ) ) {
          foreach ( $tags as $tag ) {
            $options[$tag->term_id] = $tag->name;
          }
        }

      break;

      case 'menus':
      case 'menu':

        $menus = wp_get_nav_menus( $query_args );

        if ( ! is_wp_error( $menus ) && ! empty( $menus ) ) {
          foreach ( $menus as $menu ) {
            $options[$menu->term_id] = $menu->name;
          }
        }

      break;

      case 'post_types':
      case 'post_type':

        $post_types = get_post_types( array(
          'show_in_nav_menus' => true
        ) );

        if ( ! is_wp_error( $post_types ) && ! empty( $post_types ) ) {
          foreach ( $post_types as $post_type ) {
            $options[$post_type] = ucfirst($post_type);
          }
        }

      break;

      case 'users':

        $users = get_users( $query_args );

        if ( ! is_wp_error( $users ) && ! empty( $users ) ) {
          foreach ( $users as $user ) {
            $options[$user->ID] = $user->data->display_name;
          }
        }

        return $options;

      break;      

    }

        return $options;

      }

  		
  	}



}

?>