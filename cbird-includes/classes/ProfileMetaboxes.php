<?php

namespace Cbird;

Class ProfileMetaboxes {
	
	private $options;
	private $metakey;
	private $saved_values = array();

	public function __construct() {

	}


	public function create($options) {

		$this->options = $options;
		$this->metakey = $options['id'];

		add_action('user_new_form', array(&$this,'init_metaboxes')); 
		add_action('show_user_profile', array(&$this,'init_metaboxes')); 
    	add_action('edit_user_profile', array(&$this,'init_metaboxes'));

    	add_action('user_register', array(&$this,'save'));
    	add_action('personal_options_update', array(&$this,'save'));
    	add_action('edit_user_profile_update', array(&$this,'save'));

	}

	public function init_metaboxes($user){

        if ( !current_user_can( 'administrator', $user->id ) ) {
            return false;
        }

        $this->render($user->id);

 	}
	
	public function render($user_id) {

		/*
		 * Load $_POST values ​​if user cannot be created
		 */

		if(isset($_POST['action'])) {
			$this->saved_values = $_POST['cbird_meta'];
		} else {
			$this->saved_values = get_the_author_meta( $this->metakey, $user_id );
		}

		if(!empty($this->options['fields'])) {

			foreach ($this->options['fields'] as $field) {

				if(class_exists('Cbird_Meta_Boxes_Field_' . $field['type'])) {

						$className = 'Cbird_Meta_Boxes_Field_' . $field['type'];

						// Check if there is any value saved

						if(isset($field['id']) && isset($this->saved_values[$field['id']])) {
							$field['saved_value'] = $this->saved_values[$field['id']];
						} else {
							$field['saved_value'] = '';
						}

						if(isset($field['id'])){
							$field['id'] = 'cbird_meta[' . $field['id'] . ']';
						}

						// Echo the field
						$theField = New $className($field);

				} else {
					echo "Field dosen't exist";
				}

			}

		}

	}


    function save( $user_id ) {

        if ( !current_user_can( 'administrator', $user_id ) ) {
        	return false;
        }

        update_usermeta( $user_id, $this->metakey, $_POST['cbird_meta'] );

    }	

}

?>