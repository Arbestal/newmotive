<?php

namespace Cbird;

Class Start {
	
	public $theme_options;
	public $theme_settings;

	public function __construct() {

		// Load settings
		$this->theme_settings = CBIRD_THEME_SETTINGS;

		// Load the metaboxes
      	$located_fields = array();

	    foreach ( glob( ROOT .'/cbird-includes/metaboxes/fields/*/*.php' ) as $file ) {
	    	$fields[] = basename( $file );
	    }

	    if( !empty($fields) ) {
	    	foreach ($fields as $file) {
	    		$folder = str_replace('.php', '', $file);
	    		include(ROOT . '/cbird-includes/metaboxes/fields/' . $folder . '/' . $file);
	    	}
	    }

		// Load the stylesheet for metaboxes
	    add_action( 'admin_enqueue_scripts', array(&$this,'admin_scripts_and_styles'));
		add_action( 'wp_enqueue_scripts', array(&$this,'scripts_and_styles'));
		add_action( 'init', array(&$this,'init'));
		// add_filter( 'allowed_block_types', array(&$this,'gutenbergBlocks'));

		// Load option
		$this->theme_options = get_option('cbird_theme_settings');

		/* Check if the adminbar should be visible in frontend */
		if( isset($this->theme_options['hide_admin_bar']['yes']) ) {
			add_filter('show_admin_bar', '__return_false');
		}

		/* Menus */
		add_theme_support('menus');
		
		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );
		add_theme_support( 'post-thumbnails' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

    	/* Add custom style for the loginpage */
    	add_action('login_enqueue_scripts', array(&$this,'style_login_screen'));
   	
    	// Add widgetareas
		add_action( 'widgets_init', array(&$this,'widgets_init') );
	}


	public function init() {

		// Remove all core patterns		
		unregister_block_pattern('core/two-buttons');
		unregister_block_pattern('core/three-buttons');
		unregister_block_pattern('core/text-two-columns');
		unregister_block_pattern('core/text-two-columns-with-images');
		unregister_block_pattern('core/text-three-columns-buttons');
		unregister_block_pattern('core/two-images');
		unregister_block_pattern('core/large-header');
		unregister_block_pattern('core/large-header-button');
		unregister_block_pattern('core/quote');
		unregister_block_pattern('core/heading-paragraph');

		/*
		 * Contact Person
		 */

		$asset_file =  ROOT . 'blocks/contact-person/contact-person.asset.php';

		

	    register_nav_menus(array(
			'mainmenu' => __('Huvudmeny'),
		));
		
		

	}


	public function admin_scripts_and_styles() {

		wp_register_style('cbird_meta_boxes', get_template_directory_uri() . '/cbird-includes/css/Metaboxes.css', array(), '1.0', 'all');
        wp_enqueue_style('cbird_meta_boxes');

		wp_register_style('cbird_gutenberg', get_template_directory_uri() . '/cbird-includes/css/gutenberg.css', array(), '1.0', 'all');
        wp_enqueue_style('cbird_meta_boxes');


        wp_register_script('cbird_meta_boxes_js', get_template_directory_uri() . '/cbird-includes/js/metaboxes.js', array('jquery', 'wp-color-picker', 'cbird_dependency'), '1.8.0', true);
        wp_enqueue_script('cbird_meta_boxes_js');

        wp_register_script('cbird_dependency', get_template_directory_uri() . '/cbird-includes/js/deps.js', array('jquery'), '1.8.0', true);
        wp_enqueue_script('cbird_dependency');

        wp_enqueue_style( 'wp-color-picker' ); 

		wp_register_style('font_awsome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '1.0', 'all');
        wp_enqueue_style('font_awsome');

		wp_register_style('bootstrap_multiselect_css', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css', array(), '1.0', 'all');
        wp_enqueue_style('bootstrap_multiselect_css');

		wp_register_style('bootstrap_css', get_stylesheet_directory_uri() .'/css/bootstrap.css', array(), '1.0', 'all');
        wp_enqueue_style('bootstrap_css');

		wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '1.8.0', true);
        wp_enqueue_script('bootstrap');

		wp_register_script('bootstrap_multiselect', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js', array('jquery'), '1.8.0', true);
        wp_enqueue_script('bootstrap_multiselect');
		


	}


	public function scripts_and_styles() {

		if(!$this->theme_settings['cache']['js']) {
			$flag = time();
		} else {
			$flag = $this->theme_settings['version'];
		}

		wp_register_script('slick-slider', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1.0', 'all');
        wp_enqueue_script('slick-slider');

		wp_register_style('slick-slider', get_template_directory_uri() . '/css/slick.css', array(), '1.0', 'all');
        wp_enqueue_style('slick-slider');

		wp_register_script('main_scripts', get_template_directory_uri() . '/js/script.js', array('jquery'), $flag, 'all');
		wp_localize_script('main_scripts', 'wp_data', array( 
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'home_url' => home_url(),
			'nonce' => wp_create_nonce('ajax-nonce'),
			) );
        wp_enqueue_script('main_scripts');

		wp_register_script('cbird_scripts', get_template_directory_uri() . '/cbird-includes/js/cbird-scripts.js', array('jquery'), '1.0', 'all');
        wp_enqueue_script('cbird_scripts');

		wp_register_style('cbird_reset', get_template_directory_uri() . '/cbird-includes/css/reset.css', array(), '1.0', 'all');
        wp_enqueue_style('cbird_reset');

		/*
		 * Check if the css should be cached
		 */

		if(!$this->theme_settings['cache']['css']) {
			$flag = time();
		} else {
			$flag = $this->theme_settings['version'];
		}

		wp_register_style('cbird_style', get_template_directory_uri() . '/style.css', array(), $flag, 'all');
        wp_enqueue_style('cbird_style');

	}


	public function style_login_screen() {

		$option = get_option('cbird_theme_settings');
		$logo = cbird_get_image_src($option['login_logo']);

		?> 
		<style type="text/css">

		body {
			background: <?php echo $option['login_background']; ?> !important;
		}

		.login #backtoblog a, 
		.login #nav a {
			color: <?php echo $option['login_textcolor']; ?> !important;
		}

		body.login div#login h1 a {
			background-image: url(<?php echo $logo; ?>);
			padding-bottom: 30px; 
			background-repeat: no-repeat;
			width: 100%;
		    background-size: contain;
		    background-position: center;	
			max-width: 160px;
		} 
		</style>
		<?php 


	}


	/**
	 * Add the themes widgetareas here
	 *
	 */
	public function widgets_init() {

		register_sidebar( array(
			'name'          => 'Adress i sidfot',
			'id'            => 'footer_widget',
			'description' 	=> __('Widget-yta i sidfooten'),
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
			'before_title'  => '<strong>',
			'after_title'   => '</strong>',
		) );

	}
	

	public function gutenbergBlocks() {

		$gutenberg_blocks = array_merge($this->theme_settings['custom_gutenberg_blocks'], $this->theme_settings['core_gutenberg_blocks']);

		$allowed_blocks = array();

		if( isset($gutenberg_blocks) ) {

			if( !empty($gutenberg_blocks) ) {

				foreach ($gutenberg_blocks as $block_name => $show_block) {
					if($show_block) {
						$allowed_blocks[] = $block_name;
					}
				}

			}

		}

		return $allowed_blocks;
	
	}


} // End of class