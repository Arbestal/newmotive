<?php

namespace Cbird;

Class CategoryMetaboxes {
	
	private $options;
	private $metakey;
	private $saved_values = array();
	private $category;

	public function __construct() {

	}

	public function create($options) {

		$this->options = $options;
		$this->metakey = $options['id'];

		$this->category = $options['category'] ?? 'category';

		add_action( $this->category . '_add_form_fields', array(&$this, 'render'), 100 );
		add_action( $this->category . '_edit_form_fields', array(&$this, 'render'), 100 );

	    add_action( 'save_post', array(&$this,'save'), 10, 2 );

	    add_action( 'edited_' . $this->category, array(&$this,'save'), 10, 2 );
		add_action( 'create_' . $this->category, array(&$this,'save'), 10, 2 );

		// Add custom script to the category page
		add_action( 'admin_enqueue_scripts', array(&$this,'add_admin_js'), 10, 1 );


	}


	public function add_admin_js($hook) {

	    global $post;

	    if ( $hook == 'edit-tags.php' ) {
	        
	            wp_enqueue_script(  'cbird-metaboxes-category.js', get_stylesheet_directory_uri().'/cbird-includes/js/metaboxes-category.js' );
	        
	    }

	}

	public function render($term) {

		// Get saved values
		
		$term_meta = get_term_meta($term->term_id);

		if(isset($term_meta['cbird_meta'][0])) {

			$term_meta = $term_meta['cbird_meta'][0] ?? array();
			$term_meta = unserialize($term_meta);
			$this->saved_values = $term_meta;
				
			$field['show_default'] = false;
			
		} else {

			$field['show_default'] = true;

		}

		if(!empty($this->options['fields'])) {

			foreach ($this->options['fields'] as $field) {
				if(class_exists('Cbird_Meta_Boxes_Field_' . $field['type'])) {


						$className = 'Cbird_Meta_Boxes_Field_' . $field['type'];

						// Check if there is any value saved

						if(isset($this->saved_values[$field['id']])) {
							$field['saved_value'] = $this->saved_values[$field['id']];
						} else {
							$field['saved_value'] = '';
						}

						if(isset($field['id'])){
							$field['dep_id'] = $field['id'];
							$field['id'] = 'cbird_meta[' . $field['id'] . ']';
						}

						// Echo the field
						$theField = New $className($field);

				} else {
					echo "Field dosen't exist";
				}

			}

		}

	}

	public function save($term_id) {

		if (isset( $_POST['cbird_meta'] ) && is_array( $_POST['cbird_meta'] )) {

			update_term_meta( $term_id, 'cbird_meta', $_POST['cbird_meta']);

		}


	}


}