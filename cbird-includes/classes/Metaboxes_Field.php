<?php
namespace Cbird;

Class Metaboxes_Field {

	  private $value;
	  private $id;
	  private $name;
	  private $title;
	  private $options;
	  private $show_default;
    private $default;
    private $description;
    private $query_args;
    private $query_type;
    private $attributes;
    private $checked;

  public function __construct($field) {

    GLOBAL $pagenow;
    $this->clean_id = $field['id'] ?? '';
  	$this->id = $field['id'] ?? '';
    $this->dep_id = $field['dep_id'] ?? '';
  	$this->value = $field['saved_value'] ?? '';
  	$this->options = $field['options'] ?? array();
    $this->default = $field['default'] ?? '';
    $this->description = $field['description'] ?? '';
  	$this->show_default = $field['show_default'] ?? '';
  	$this->show_side = $field['show_side'] ?? 'false';
  	$this->title = $field['title'] ?? '';
    $this->query_args = $field['query_args'] ?? array();
    $this->query_type = $field['query_type'] ?? 'page';
    $this->attributes = $field['attributes'] ?? array();
    $this->checked = $field['checked'] ?? '';
    $this->dependency = $field['dependency'] ?? array();
    $this->fields = $field['fields'] ?? array();
    $this->metakey = $field['metakey'] ?? '';

    $this->page = $pagenow;

  	$this->render();

  }

  public function id() {
  	return $this->id;
  }

  public function title() {
  	return $this->title;
  }

  public function query_args() {
  	return $this->query_args;
  }

  public function description() {
    return $this->description;
  }

  public function attributes($type = 'string') {
    if($type == 'array') {
      if(!is_array($this->attributes)) {
        return array();
      } else {
        return $this->attributes;
      }
    } else {
      return $this->attributes;  
    }
  }

  public function value($type = 'string') {

  	if($this->show_default && $this->page == 'post-new.php') {
      
  		return $this->default;

  	}

  	if($type == 'array') {
  		if(!is_array($this -> value)) {
  			return array();
  		} else {
  			return $this -> value;
  		}
  	} else {
  		return $this -> value;	
  	}
  	
  }

  public function default() {
  		return $this->default();
  }

  public function options() {

  	if(!empty($this->options) && empty($this->query_args)) {

  		return $this->options;	

  	} else {

      // Check for query_args 

      if(empty($this->query_args)) {

        return array();  

      } else {


      $options = [];


      switch( $this->query_type ) {

      case 'pages':
      case 'page':

        $pages = get_pages( $this->query_args );

        if ( ! is_wp_error( $pages ) && ! empty( $pages ) ) {
          foreach ( $pages as $page ) {
            $options[$page->ID] = $page->post_title;
          }
        }

      break;

      case 'products':
      case 'product':

        if($this->query_args['grouped']) {

          $terms = get_terms([
            'taxonomy' => 'product_cat',
            'hide_empty' => false,
            'posts_per_page' => -1,
          ]);

          $select = array();

            if(!empty($terms)) {
                foreach ($terms as $term) {

                    $posts = get_posts(array(
                        'post_type' => 'product',
                        'posts_per_page' => -1,
                        'orderby' 		=> 'title',
                        'order' 		=> 'ASC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_cat',
                                'terms' => array( $term->term_id ),
                                'field' => 'term_id',
                            )
                        ),
                    ));


                    $select[] = array(
                        'name' => $term->name,
                        'posts' => $posts
                    );

                }
            }

            return $select;

        }


      break;



      case 'posts':
      case 'post':

        $posts = get_posts( $this->query_args );

        if ( ! is_wp_error( $posts ) && ! empty( $posts ) ) {
          foreach ( $posts as $post ) {
            $options[$post->ID] = $post->post_title;
          }
        }

      break;

      case 'categories':
      case 'category':

        $categories = get_categories( $this->query_args );

        if ( ! is_wp_error( $categories ) && ! empty( $categories ) && ! isset( $categories['errors'] ) ) {
          foreach ( $categories as $category ) {
            $options[$category->term_id] = $category->name;
          }
        }

      break;

      case 'tags':
      case 'tag':

        $taxonomies = ( isset( $query_args['taxonomies'] ) ) ? $query_args['taxonomies'] : 'post_tag';
        $tags = get_terms( $taxonomies, $query_args );

        if ( ! is_wp_error( $tags ) && ! empty( $tags ) ) {
          foreach ( $tags as $tag ) {
            $options[$tag->term_id] = $tag->name;
          }
        }

      break;

      case 'menus':
      case 'menu':

        $menus = wp_get_nav_menus( $query_args );

        if ( ! is_wp_error( $menus ) && ! empty( $menus ) ) {
          foreach ( $menus as $menu ) {
            $options[$menu->term_id] = $menu->name;
          }
        }

      break;

      case 'post_types':
      case 'post_type':

        $post_types = get_post_types( array(
          'show_in_nav_menus' => true
        ) );

        if ( ! is_wp_error( $post_types ) && ! empty( $post_types ) ) {
          foreach ( $post_types as $post_type ) {
            $options[$post_type] = ucfirst($post_type);
          }
        }

      break;

      case 'users':



        $users = get_users( $this->query_args );

        if ( ! is_wp_error( $users ) && ! empty( $users ) ) {
          foreach ( $users as $user ) {
            $this->options[$user->ID] = $user->data->display_name;
          }
        }

        return $this->options;

      break;      

    }

        return $options;

      }

  		
  	}
  }

  public function checked( $helper = '', $current = '', $type = 'checked', $echo = false ) {

    if ( is_array( $helper ) && in_array( $current, $helper ) ) {
      $result = ' '. $type .'="'. $type .'"';
    } else if ( $helper == $current ) {
      $result = ' '. $type .'="'. $type .'"';
    } else {
      $result = '';
    }

    if ( $echo ) {
      echo $result;
    }

    return $result;

  }



  public function before() { 

    if( empty($this->dependency) ) { ?>

      <div class="cbird_meta_box" id="<?php echo $this->id(); ?>">
      <div class="cbird_meta_box_title">
        <?php echo $this->title(); ?>
      </div>
      <div class="cbird_meta_box_field"> 


    <?php } else { ?>

      <div class="cbird_meta_box" data-depend-id="<?php echo $this->id(); ?>" id="<?php echo $this->id(); ?>" data-controller="<?php echo $this->dependency[0]; ?>" data-condition="<?php echo $this->dependency[1]; ?>" data-value="<?php echo $this->dependency[2]; ?>">
      <div class="cbird_meta_box_title">
        <?php echo $this->title(); ?>
      </div>
      <div class="cbird_meta_box_field"> 

    <?php }

   		
  } // End of before

  public function after() { ?>
  		</div>
		</div>
  <?php }
	
}
?>