<?php
namespace Cbird;

/* Options */
$frontpage = New Options(array(
	'title' => 'Test Inställningar',
	'id' => 'test_settings',
	'slug' => 'test_settings',
	'capability' => 'manage_options',
	'icon' => 'dashicons-heart',
	'fields' =>  array(
		array(
			'type'  => 'heading',
			'title' => 'Inställningar'
        ),
    	array(
            'id'    => 'text',
            'type'  => 'text',
            'title' => 'Inputfält'
        ), 
	),

));
?>