<?php
namespace Cbird;

/* Options */
$test = New Options(array(
    'title' => 'Temainställningar',
    'id' => 'cbird_theme_settings',
    'slug' => 'theme-settings',
    'capability' => 'manage_options',
    'icon' => 'dashicons-heart',
    'position' => '2',
    //'type' => 'options',
    'fields' =>  array(
       

        array(
                'type' => 'heading',
                'title' => 'Sidfot'
        ),


        array(
                'id' => 'contact_info',
                'type' => 'wysiwyg',
                'title' => 'Kontaktuppgifter i sidfoten'
        ),

        array(
                'type' => 'section-start',
                'title' => 'About us'
        ),

        array(
                'id' => 'footer_box1_headline',
                'type' => 'text',
                'title' => 'Rubrik'
        ),

        array(
                'id' => 'footer_box1_text',
                'type' => 'textarea_small',
                'title' => 'Text'
        ),

        array(
                'id' => 'footer_box1_link',
                'type' => 'text',
                'title' => 'Länk'
        ),

        array(
                'id' => 'footer_box1_linktext',
                'type' => 'text',
                'title' => 'Länktext'
        ),

        array(
                'type' => 'section-end',
        ),

        array(
                'type' => 'section-start',
                'title' => 'Footer info'
        ),

        array(
                'id' => 'footer_box2_headline',
                'type' => 'text',
                'title' => 'Rubrik'
        ),

        array(
                'id' => 'footer_box2_text',
                'type' => 'wysiwyg',
                'title' => 'Text'
        ),

        array(
                'id' => 'footer_box2_link',
                'type' => 'text',
                'title' => 'Länk'
        ),

        array(
                'id' => 'footer_box2_linktext',
                'type' => 'text',
                'title' => 'Länktext'
        ),

        array(
                'type' => 'section-end',
        ),        


        array(
                'type' => 'section-start',
                'title' => 'Cookies'
        ),
        array(
                'id' => 'show_cookie',
                'type' => 'radio',
                'title' => 'Visa cookiebar',
                'options' => array(
                    'true' => 'Visa',
                    'false' => 'Visa inte',
                )
        ),
        
        array(
                'id' => 'cookie_text',
                'type' => 'wysiwyg',
                'title' => 'Meddelande',
                'default' => 'En cookie är en liten datafil med text som sparas i din webbläsare. Svensk lag kräver att vi informerar om vilka cookies som används samt deras syfte.<br>
                Vi använder cookies för att kunna ge dig som besökare av vår webbplats bättre service: för att spara statistik, för att kunna följa hur besökare navigerar och på så sätt bli mer relevanta. '

        ),
        array(
                'id' => 'cookie_text_bg',
                'type' => 'colorpicker',
                'title' => 'Bakgrundsfärg'
        ),
        array(
                'id' => 'cookie_text_color',
                'type' => 'colorpicker',
                'title' => 'Textfärg'
        ),
        
        array(
                'id' => 'cookie_btn_text',
                'type' => 'text',
                'title' => 'Text på knappen'
        ), 
        array(
                'id' => 'cookie_btn_bg',
                'type' => 'colorpicker',
                'title' => 'Bakgrundsfärg på knappen'
        ),
        array(
                'id' => 'cookie_btn_color',
                'type' => 'colorpicker',
                'title' => 'Färg på knapptexten'
        ),

        /* SOCIAL MEDIA */
        array(
                'type' => 'section-start',
                'title' => 'Sociala medier'
        ),
        array(
                'id' => 'social_facebook',
                'type' => 'text',
                'title' => 'Url till Facebook-konto'
        ),
        array(
                'id' => 'social_linkedin',
                'type' => 'text',
                'title' => 'Url till linkedin-konto'
        ),  
        array(
                'id' => 'social_instagram',
                'type' => 'text',
                'title' => 'Url till Instagram-konto'
        ), 
        array(
               'id' => 'social_vimeo',
                'type' => 'text',
                'title' => 'Url till Vimeo-konto'
        ),
        array(
                'type' => 'section-end',
        ),

        /* LOGIN PAGE */
        array(
                'type' => 'section-start',
                'title' => 'Inloggningssida'
        ),
        array(
                'id' => 'login_background',
                'type' => 'colorpicker',
                'title' => 'Bakgrundfärg inloggningssida'
        ), 
        array(
                'id' => 'login_textcolor',
                'type' => 'colorpicker',
                'title' => 'Textfärg inloggningssida'
        ),         
        array(
                'id' => 'login_logo',
                'type' => 'image',
                'title' => 'Logo inloggningssida'
        ), 
        array(
                'type' => 'section-end',
        ),

        /* OTHER */
        array(
                'type' => 'section-start',
                'title' => 'Övrigt'
        ),
        array(
                'id' => '404-page',
                'type' => 'select',
                'title' => '404-sida',
                'query_args'    => array(
                    'sort_order'  => 'ASC',
                    'post_type'    => 'page',
                    'sort_column' => 'post_title',
                ),
                'attributes'    => array(
                        'placeholder' 	=> 'Välj sida',
                        'style'       	=> 'width: 230px;height:30px;'
                ),
        ), 
        array(
                'id' => 'hide_admin_bar',
                'type' => 'checkbox',
                'title' => 'Dölj adminbar i frontend',
                'options' => array(
                    'yes' => 'Dölj adminbar'
                )
        ),
        array(
                'type' => 'section-end',
        ),
    ),

));