<?php
namespace Cbird;

/* Options */
$test = New Options(array(
    'title' => 'Startsida',
    'id' => 'cbird_frontpage_settings',
    'slug' => 'frontpage-settings',
    'capability' => 'manage_options',
    'icon' => 'dashicons-admin-home',
    'position' => '2',
    //'type' => 'options',
    'fields' =>  array(
        array(
            'type' => 'heading',
            'title' => 'Startsida'
        ),

        /* OTHER */
        array(
            'type' => 'section-start',
            'title' => 'Bildspel'
        ),


        array(
            'id' => 'slider',
            'type' => 'boxes',
            'title' => 'Bildspel',
            'fields' => array(
                array(
                   'id'    => 'headline',
                   'type'  => 'text',
                   'title' => 'Rubrik',
                   ),
                   array(
                    'id'    => 'background-color',
                    'type'  => 'radio',
                    'title' => 'Bakgrundsfärg',
                    'default' => 'light',
                    'show_default' => true,
                    'options' => array(
                        'light' => 'Ljus',
                        'dark' => 'Mörk',
                    )
                ),                   
                array(
                    'id'    => 'image',
                    'type'  => 'image',
                    'title' => 'Bild',
                ),
                array(
                    'id'    => 'icon',
                    'type'  => 'image',
                    'title' => 'Ikon',
                ),
                array(
                    'id'    => 'link',
                    'type'  => 'text',
                    'title' => 'Länk',
                ),
                array(
                    'id'    => 'linktext',
                    'type'  => 'text',
                    'title' => 'Knapptext',
                ),
               
        ),
        ),


        array(
            'id' => 'puffs',
            'type' => 'boxes',
            'title' => 'Puffar',
            'fields' => array(
                array(
                   'id'    => 'headline',
                   'type'  => 'text',
                   'title' => 'Puffar',
                   ),                 
                array(
                    'id'    => 'image',
                    'type'  => 'image',
                    'title' => 'Bild',
                ),
                array(
                    'id'    => 'link',
                    'type'  => 'text',
                    'title' => 'Länk',
                ),
                array(
                    'id'    => 'linktext',
                    'type'  => 'text',
                    'title' => 'Knapptext',
                ),

        ),
    ),

    array(
        'id' => 'products',
        'type' => 'multiselect',
        'title' => 'Produkter',
        'query_type' => 'product',
        'query_args' => array(
            'post_type' => 'product',
            'numberposts' => -1,
            'grouped' => true,
        ),
      ),  

    array(
        'id' => 'recipes',
        'type' => 'multiselect',
        'title' => 'Recept',
        'query_type' => 'post',
        'query_args' => array(
            'post_type' => 'recipe',
            'numberposts' => -1,
            'grouped' => false,
        ),
      ),  

        array(
                'type' => 'section-end',
        ),
    ),

));