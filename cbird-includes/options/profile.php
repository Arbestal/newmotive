<?php

namespace Cbird;

$profile_metabox = New ProfileMetaboxes();

$profile_metabox->create(array(
    'id' => 'profile_meta',
    'title' => 'Inställningar',
    'fields' => array(
        array(
            'type' => 'heading',
            'title' => 'Inställningar'
        ),
        array(
            'id' => 'title',
            'type' => 'text',
            'title' => 'Titel',
        ),        
        array(
            'id' => 'phone',
            'type' => 'text',
            'title' => 'Telefonnummer',
        ),
        array(
            'id' => 'order',
            'type' => 'text',
            'title' => 'ordning',
        ),    
        array(
            'id' => 'show',
            'type' => 'checkbox',
            'title' => 'Kontaktperson',
            'options' => array(
                'yes' => 'Visa på kontaktsidan'
            )
        ),
    )
));