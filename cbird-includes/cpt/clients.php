<?php
namespace Cbird;

/* Create Custom Post Types */

add_action('init', function(){
/* Post Type */
    $labels = array(
        'name' => _x('Kunder', cbird_text_domain),
        'singular_name' => _x('Kund', cbird_text_domain),
        'add_new' => _x('Lägg till', cbird_text_domain),
        'add_new_item' => __('Lägg till ny kund', cbird_text_domain),
        'edit_item' => __('Redigera kund' , cbird_text_domain),
        'new_item' => __('Ny kund', cbird_text_domain),
        'view_item' => __('Visa kund', cbird_text_domain),
        'search_items' => __('Sök kund', cbird_text_domain),
        'not_found' =>  __('Fanns inte', cbird_text_domain),
        'not_found_in_trash' => __('Ingenting i papperskorgen', cbird_text_domain),
    );
 
    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'query_var'             => true,
        'show_in_rest'          => true,
        'rewrite'               => array( 'slug' => 'clients' ),
        'capability_type'       => 'post',
        'hierarchical'          => true,
        'has_archive'           => true,
        'show_ui'               => true,
        'menu_icon'             => 'dashicons-admin-site-alt2',
        'taxonomies'            => array( '' ),
        'supports'              => array('title', 'permalink', 'thumbnail'),
      ); 


    register_post_type( 'clients' , $args );

    

    
});