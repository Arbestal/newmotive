<?php
namespace Cbird;

/* Create Custom Post Types */

add_action('init', function(){

  /* Custom taxonomy */

  $labels = array(
    'name' => _x( 'Taxonomy', cbird_text_domain ),
    'singular_name' => _x( 'Taxonomy', cbird_text_domain ),
    'search_items' =>  __( 'Search Taxonomy', cbird_text_domain ),
    'all_items' => __( 'All Taxonomy', cbird_text_domain ),
    'parent_item' => __( 'Taxonomy Parent', cbird_text_domain ),
    'parent_item_colon' => __( 'Taxonomy Parent:', cbird_text_domain ),
    'edit_item' => __( 'Edit Taxonomy', cbird_text_domain ), 
    'update_item' => __( 'Update Taxonomy', cbird_text_domain ),
    'add_new_item' => __( 'Add New Taxonomy', cbird_text_domain ),
    'new_item_name' => __( 'New Taxonomy', cbird_text_domain ),
    'menu_name' => __( 'My Custom Taxonomy', cbird_text_domain ),
  );    
  
  register_taxonomy('my_custom_taxonomy',array(), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'my_custom_taxonomy' ),
  ));


  /* Post Type */

    $labels = array(
        'name' => _x('Posttype', cbird_text_domain),
        'singular_name' => _x('Posttype', cbird_text_domain),
        'add_new' => _x('Add New', cbird_text_domain),
        'add_new_item' => __('Add New Post', cbird_text_domain),
        'edit_item' => __('Edit Post' , cbird_text_domain),
        'new_item' => __('New Post', cbird_text_domain),
        'view_item' => __('View Post', cbird_text_domain),
        'search_items' => __('Search Post', cbird_text_domain),
        'not_found' =>  __('Not found', cbird_text_domain),
        'not_found_in_trash' => __('Nothing in the trash', cbird_text_domain),
    );
 
    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'my_posttype' ),
        'capability_type'       => 'post',
        'hierarchical'          => true,
        'has_archive'           => true,
        'show_ui'               => true,
        'menu_icon'             => 'dashicons-hammer',
        'taxonomies'            => array( 'my_custom_taxonomy' ),
        'supports'              => array('title', 'permalink', 'editor', 'thumbnail'),
      ); 


    register_post_type( 'my_posttype' , $args );

/* Create metaboxes for the posttype */

    $metabox = New Metaboxes();

    $metabox -> create(array(
        'id' => 'my_custom_post_meta_id',
        'post-type' => 'my_posttype',
        'type' => 'normal',
        'title' => 'Settings',
        'fields' => array(
            array(
                'type' => 'heading',
                'title' => 'Heading'
            ),            
            array(
                'id' => 'text_id',
                'type' => 'text',
                'title' => 'Textfield',
            ),
            array(
                'id' => 'image_id',
                'type' => 'image',
                'title' => 'Image'
            ),
            array(
                'id' => 'colorpicker_id',
                'type' => 'colorpicker',
                'title' => 'Colorpicker'
            ),
            array(
                'id' => 'textarea_id',
                'type' => 'textarea',
                'title' => 'Textarea'
            ),
            array(
                'id' => 'radio_id',
                'type' => 'radio',
                'title' => 'Radio',
                'options' => array(
                    'option1' => 'Option 1',
                    'option2' => 'Option 2',
                    'option3' => 'Option 3',
                )
            ),
            array(
                'id' => 'checkbox_id',
                'type' => 'checkbox',
                'title' => 'Checkbox',
                'options' => array(
                    'option1' => 'Option 1',
                    'option2' => 'Option 2',
                    'option3' => 'Option 3',
                )
            ),            
            array(
                'id' => 'select_id',
                'type' => 'select',
                'title' => 'Select',
                'options' => array(
                    'option1' => 'Option 1',
                    'option2' => 'Option 2',
                    'option3' => 'Option 3',
                )
            ),               
        )
    ));

/* Create metaboxes for the custom taxonomy */

    $metabox = New CategoryMetaboxes();

    $metabox -> create(array(
        'id' => 'my_custom_taxonomy_meta_id',
        'category' => 'my_custom_taxonomy',
        'type' => 'normal',
        'title' => 'Settings',
        'fields' => array(
            array(
                'id' => 'image_id',
                'type' => 'image',
                'title' => 'Image'
            ),
        )
    ));

});


?>