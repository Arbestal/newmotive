<?php
namespace Cbird;

/* Create Custom Post Types */

add_action('init', function(){
/* Post Type */
    $labels = array(
        'name' => _x('Projekt', cbird_text_domain),
        'singular_name' => _x('Projekt', cbird_text_domain),
        'add_new' => _x('Lägg till', cbird_text_domain),
        'add_new_item' => __('Lägg till nytt projekt', cbird_text_domain),
        'edit_item' => __('Redigera projekt' , cbird_text_domain),
        'new_item' => __('Nytt projekt', cbird_text_domain),
        'view_item' => __('Visa projekt', cbird_text_domain),
        'search_items' => __('Sök projekt', cbird_text_domain),
        'not_found' =>  __('Fanns inte', cbird_text_domain),
        'not_found_in_trash' => __('Ingenting i papperskorgen', cbird_text_domain),
    );
 
    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'query_var'             => true,
        'show_in_rest'          => true,
        'rewrite'               => array( 'slug' => 'projects' ),
        'capability_type'       => 'post',
        'hierarchical'          => true,
        'has_archive'           => true,
        'show_ui'               => true,
        'menu_icon'             => 'dashicons-admin-site-alt2',
        'taxonomies'            => array( '' ),
        'supports'              => array('title', 'permalink', 'editor', 'thumbnail'),
      ); 


    register_post_type( 'projects' , $args );

    
$metabox = New Metaboxes();


$metabox -> create(array(
    'id' => 'page_meta',
    'post-type' => 'projects',
    'type' => 'normal',
    'title' => 'Settings',
    'fields' => array(
        array(
            'type' => 'heading',
            'title' => 'Settings'
        ),            
        array(
            'id' => 'client',
            'type' => 'text',
            'title' => 'Kund'
        ),
        array(
            'id' => 'color',
            'type' => 'radio',
            'title' => 'Färg på kund',
            'options' => array(
                'white' => 'White',
                'blackish' => 'Blackish',
            ),
            'default' => 'white'
        ),
    )
));

    
});