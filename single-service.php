<?php get_header(); ?>

	<?php 
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); 

            if( has_post_thumbnail() ) { ?>

                <div class="topimage" style="background-image: url(<?php echo the_post_thumbnail_url('Toppbild'); ?>">
                    <div class="topimage__content">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>

            <?php } else { ?>

            <?php } ?>
                
                <div class="content">
                    <?php nl2br(the_content()); ?>
                </div>
			<?php

			//
		} // end while
	} // end if
	?>



<?php get_footer(); ?>