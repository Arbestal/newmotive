(function($) {

    /*
     * Cookie
     */

    var acceptCookies = Cookies.get('acceptCookies');
    if (acceptCookies != "true") {
        $('#cb-cookie-holder').fadeIn();
        $('#cb-cookie-accept').click(function(){
            $('#cb-cookie-holder').fadeOut();
            Cookies.set('acceptCookies', 'true', { expires: 365 });
        });
    }

	$('#contactform').submit(function(e){
		e.preventDefault();
		$('#contactform__loader').css('display', 'flex').hide().fadeIn(function(){

			$.ajax({
				url : wp_data.ajax_url,
				dataType: 'json',
				type: "POST",
				data: {
					action: 'adamant_contact',
					nonce: wp_data.nonce,
					name: $('#contact_name').val(),
					email: $('#contact_email').val(),
					message: $('#contact_message').val(),
					phone: $('#contact_phone').val(),
					contact: $('#contact_fax').is(':checked'),
				},
					
					success: function(data) {
						$('#contactform__loader').fadeOut();
						$('#contactform__status--success').fadeIn();
						$('#contact_name').val('');
						$('#contact_phone').val('');
						$('#contact_email').val('');
						$('#contact_message').val('');
						$('#contactform .active').removeClass('active');

						console.log(data);
					},
		
					error: function (data) {
					  console.log(data);
					}
				}); 


		});

	});

	$('#service__loadmore').click(function(){

		
		let toLoad = 6;
		let i = 1;

		$('.service--hidden').each(function(){
			
			$(this).fadeIn();

			$(this).removeClass('service--hidden');

			i++;
			if(i > toLoad) {
				return false;
			}
		});

		if( !$('.service--hidden').length )  {
			$('#service__loadmore').hide();
		}

	});


	$('.header__menuholder').click(function(){
		$(this).toggleClass('open');
	});
	$('.service').on('touchstart click', function(){
		$(this).toggleClass('active');
	});
	
	$('.slider__content').slick({
		arrows: false,
		autoplay: true,
  		autoplaySpeed: 5000,
		dots: true,
	});

	function checkHeader() {

		if($(window).scrollTop() > 243) {
			$('.headerholder').addClass('black');
		} else {
			$('.headerholder').removeClass('black');
		}

	}

	$(window).scroll(function(){
		checkHeader();
	});

	checkHeader();

	$('.contactform input, .contactform textarea').keyup(function(){
		if( $(this).val() == '' ) {
			$(this).removeClass('active');
		} else {
			$(this).addClass('active');
		}
	});

	document.querySelectorAll('a[href^="#"]').forEach(anchor => {
		anchor.addEventListener('click', function (e) {
			e.preventDefault();
	
			document.querySelector(this.getAttribute('href')).scrollIntoView({
				behavior: 'smooth'
			});
		});
	});

	function checkElementLocation() {
		var $window = $(window);
		var bottom_of_window = $window.scrollTop() + $window.height();
	  
		$('.to-fade-in').each(function(i) {
		  var $that = $(this);
		  var bottom_of_object = $that.position().top + $that.outerHeight();
	  
		  //if element is in viewport, add the animate class
		  if (bottom_of_window > bottom_of_object) {
			$(this).addClass('fade-in');
		  }
		});
	  }
	  // if in viewport, show the animation
	  checkElementLocation();
	  
	  $(window).on('scroll', function() {
		checkElementLocation();
	  });



})( jQuery );